-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Máy chủ: 127.0.0.1
-- Thời gian đã tạo: Th10 19, 2019 lúc 10:55 AM
-- Phiên bản máy phục vụ: 10.4.8-MariaDB
-- Phiên bản PHP: 7.2.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Cơ sở dữ liệu: `adiva`
--

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `inquiries`
--

CREATE TABLE `inquiries` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `work_status` enum('acknowledged','incompatible','ignore') COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'acknowledged',
  `agent` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `admin_note` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `inquiries`
--

INSERT INTO `inquiries` (`id`, `email`, `content`, `work_status`, `agent`, `admin_note`, `created_at`, `updated_at`) VALUES
(1, 'pete92@green.com', 'Eligendi unde aliquid aut in architecto. Ut quas labore sapiente enim totam ad voluptatem. Nesciunt dolorem magni suscipit sed accusantium distinctio commodi. Velit sunt vel voluptate voluptas rerum. Et doloremque dignissimos sequi quaerat est cumque. Quisquam ut dolorem fugit rerum fugit sint. Soluta et rerum consequatur sed explicabo optio. Quo rerum vero voluptatem aut cumque nemo aliquid sunt.', 'acknowledged', 'Mozilla/5.0 (X11; Linux x86_64; rv:6.0) Gecko/20191026 Firefox/36.0', NULL, '2019-11-06 18:52:23', '2019-11-06 21:42:06'),
(2, 'sofia.tromp@becker.com', 'Repellendus sit eos id alias. Consequatur assumenda fugiat doloremque earum explicabo dicta architecto. Aliquid explicabo cumque sit id distinctio. Rem nesciunt in placeat ea illum. Veniam explicabo non mollitia eos qui architecto itaque. Animi iusto et veritatis molestiae qui molestias laborum et. Laudantium possimus consectetur id. Quo dolorum cupiditate occaecati. Eos eum est quidem nulla iusto. Sed ut aut vitae assumenda aut.', 'acknowledged', 'Mozilla/5.0 (iPad; CPU OS 8_0_1 like Mac OS X; en-US) AppleWebKit/532.33.1 (KHTML, like Gecko) Version/4.0.5 Mobile/8B119 Safari/6532.33.1', NULL, '2019-11-06 18:52:23', '2019-11-06 21:42:06'),
(3, 'elwin.kovacek@rau.info', 'Magni enim voluptatum ut ut modi voluptatem laboriosam omnis. Dolore eos placeat quisquam quia saepe. Repudiandae quo commodi dicta error ex rem. Quibusdam possimus commodi illo magnam voluptatem quaerat. Sit laboriosam quia omnis dolores sed provident corrupti. Ducimus et ipsam sint. Quam deleniti deserunt maxime veritatis porro et maiores est. Et porro vero dolores voluptatem quam repellendus.', 'acknowledged', 'Mozilla/5.0 (compatible; MSIE 5.0; Windows NT 6.1; Trident/4.1)', NULL, '2019-11-06 18:52:23', '2019-11-06 21:42:06'),
(4, 'theodore.schneider@hotmail.com', 'Ut dolores voluptatem aut. Nulla amet voluptas iure sunt. Ut quasi libero ut tempore explicabo explicabo atque cumque. Doloremque ex tempora architecto deleniti. Facilis perferendis ut id dolores. Ipsa et est perspiciatis rerum. Officia non possimus neque explicabo alias magni error. Ut earum reiciendis est doloribus. Neque necessitatibus ut atque vero eos laboriosam ex.', 'acknowledged', 'Opera/9.57 (X11; Linux x86_64; sl-SI) Presto/2.10.338 Version/12.00', NULL, '2019-11-06 18:52:23', '2019-11-06 21:42:06'),
(5, 'myrna85@yundt.com', 'Alias perspiciatis sed odit possimus labore. Non voluptatem ex aut earum. Debitis ut minima necessitatibus et. Ut iure non reprehenderit sed dolor ut dolor. Quaerat est assumenda accusamus eveniet eaque vel. Est fugiat ullam non magni. Libero et in quia non sit et accusantium. Repellat tempora voluptas sed tempore laudantium officia. Nisi voluptas quis quaerat cumque. Totam molestiae dolor eum voluptatem enim voluptatibus non.', 'acknowledged', 'Mozilla/5.0 (compatible; MSIE 11.0; Windows NT 6.0; Trident/4.1)', NULL, '2019-11-06 18:52:23', '2019-11-06 21:42:06'),
(6, 'yrobel@gerhold.com', 'Nihil est distinctio soluta inventore. Cumque ut laborum ex velit aut rerum. Delectus deleniti sunt rem. Maiores qui explicabo et unde. Dolorem mollitia aliquid ut sit eos sint velit praesentium. Ut ut dolores eum eos. Eius esse ut eum facilis nisi corporis. Est odit amet voluptates mollitia asperiores incidunt. Vel veniam perferendis ut et ducimus eos quis. Eligendi sit vero libero temporibus. Voluptate consequatur est qui. Incidunt occaecati et autem.', 'acknowledged', 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/5341 (KHTML, like Gecko) Chrome/37.0.895.0 Mobile Safari/5341', NULL, '2019-11-06 18:52:23', '2019-11-06 21:42:06'),
(7, 'burnice57@gmail.com', 'Sunt est placeat distinctio ut mollitia et. Totam aut molestiae molestiae fugit. Aut voluptatem quis doloremque quis. Debitis iusto odit maxime architecto et rem. Maxime quibusdam at quisquam nostrum et libero sunt. Libero id nostrum accusamus ea cumque adipisci ut. Voluptatem corrupti natus libero. Similique dolorem aut omnis. Cumque architecto asperiores ut tempora optio suscipit ut sequi.', 'incompatible', 'Opera/9.58 (Windows NT 6.0; en-US) Presto/2.8.197 Version/11.00', NULL, '2019-11-06 18:52:23', '2019-11-06 21:42:22'),
(8, 'lowe.gust@tromp.com', 'Esse ex consequatur et culpa iure doloremque aperiam. Doloremque ullam dolores quo id. Quibusdam nemo illum voluptas placeat error quam saepe rem. Nihil nesciunt quasi ipsam dolor qui quia. A est corporis molestias error ab. Placeat fugiat voluptatibus qui corporis. Quidem et quia pariatur omnis voluptates. Exercitationem dolore quisquam autem omnis dolores.', 'ignore', 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_5_2 rv:5.0; en-US) AppleWebKit/534.33.4 (KHTML, like Gecko) Version/4.1 Safari/534.33.4', NULL, '2019-11-06 18:52:23', '2019-11-06 21:42:32'),
(9, 'feeney.ervin@yahoo.com', 'Rerum sint explicabo ratione in et id et. Placeat minima vel atque similique. Pariatur consequatur hic nihil et. Eaque animi autem at fugiat maiores fugiat rerum. Doloribus est mollitia voluptatibus aut soluta soluta sed. Explicabo eum aspernatur unde ea et iste eos ut. Facere omnis veritatis repudiandae iure voluptate excepturi. Possimus et laboriosam ut enim ea.', 'incompatible', 'Opera/8.92 (Windows NT 5.01; sl-SI) Presto/2.8.261 Version/12.00', NULL, '2019-11-06 18:52:23', '2019-11-06 21:42:22'),
(10, 'kathryn31@hotmail.com', 'Consectetur minus dolores eligendi nisi. Minima sunt aspernatur odit repellat id laboriosam. Laudantium aut qui quis rem quos. Est libero est et atque dolore. Perspiciatis id tempora quisquam impedit vitae. Facilis hic repudiandae enim. In itaque voluptas quo eveniet veniam dolor. Quo ut quasi ratione iusto et. Cumque sit blanditiis neque labore porro.', 'acknowledged', 'Opera/8.47 (X11; Linux i686; en-US) Presto/2.10.342 Version/11.00', NULL, '2019-11-06 18:52:23', '2019-11-06 21:42:05');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `medias`
--

CREATE TABLE `medias` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `info` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `original_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `xlarge_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `large_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `medium_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `small_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `thumbnail_path` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `post_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `medias`
--

INSERT INTO `medias` (`id`, `name`, `info`, `original_path`, `xlarge_path`, `large_path`, `medium_path`, `small_path`, `thumbnail_path`, `created_at`, `updated_at`, `post_id`) VALUES
(6, '002-管理者TOP.png', '1200x697 - 28.39 KB - image/png', 'media/original/1573462563_002-管理者TOP.png', NULL, 'media/large/1573462563_002-管理者TOP.png', 'media/medium/1573462563_002-管理者TOP.png', 'media/small/1573462563_002-管理者TOP.png', 'media/thumbnail/1573462563_002-管理者TOP.png', '2019-11-11 01:56:04', '2019-11-11 01:56:04', 10),
(11, 'mail.jpg', '1024x649 - 213.61 KB - image/jpeg', 'media/original/1573886693_mail.jpg', NULL, 'media/large/1573886693_mail.jpg', 'media/medium/1573886693_mail.jpg', 'media/small/1573886693_mail.jpg', 'media/thumbnail/1573886693_mail.jpg', '2019-11-15 23:44:54', '2019-11-15 23:44:54', 21),
(12, '002-管理者TOP.png', '1200x697 - 28.39 KB - image/png', 'media/original/1574044322_002-管理者TOP.png', NULL, 'media/large/1574044322_002-管理者TOP.png', 'media/medium/1574044322_002-管理者TOP.png', 'media/small/1574044322_002-管理者TOP.png', 'media/thumbnail/1574044322_002-管理者TOP.png', '2019-11-17 19:32:04', '2019-11-17 19:32:04', 21),
(13, '005-管理者-コンテンツ一覧.png', '1200x2072 - 299.75 KB - image/png', 'media/original/1574061622_005-管理者-コンテンツ一覧.png', NULL, 'media/large/1574061622_005-管理者-コンテンツ一覧.png', 'media/medium/1574061622_005-管理者-コンテンツ一覧.png', 'media/small/1574061622_005-管理者-コンテンツ一覧.png', 'media/thumbnail/1574061622_005-管理者-コンテンツ一覧.png', '2019-11-18 00:20:29', '2019-11-18 00:20:29', 21);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `menu_tags`
--

CREATE TABLE `menu_tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tag_id` bigint(20) UNSIGNED NOT NULL,
  `order` int(10) UNSIGNED NOT NULL,
  `shown_in_header_menu` tinyint(1) NOT NULL DEFAULT 0,
  `shown_in_feed_label` tinyint(1) NOT NULL DEFAULT 0,
  `display_text_enabled` tinyint(1) NOT NULL DEFAULT 0,
  `display_detail` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `shown_in_editor` tinyint(1) NOT NULL DEFAULT 0,
  `reverse_order` tinyint(1) NOT NULL DEFAULT 0,
  `hide_from_display` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `menu_tags`
--

INSERT INTO `menu_tags` (`id`, `tag_id`, `order`, `shown_in_header_menu`, `shown_in_feed_label`, `display_text_enabled`, `display_detail`, `shown_in_editor`, `reverse_order`, `hide_from_display`, `created_at`, `updated_at`) VALUES
(7, 14, 1, 1, 0, 0, NULL, 0, 0, 0, '2019-11-19 02:35:45', '2019-11-19 02:35:45'),
(8, 15, 2, 1, 0, 0, NULL, 0, 0, 0, '2019-11-19 02:35:53', '2019-11-19 02:35:53'),
(9, 16, 3, 1, 0, 0, NULL, 0, 0, 0, '2019-11-19 02:36:05', '2019-11-19 02:36:05'),
(10, 17, 4, 1, 0, 0, NULL, 0, 0, 0, '2019-11-19 02:36:15', '2019-11-19 02:36:15');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_10_24_082045_create_sites_table', 1),
(4, '2019_10_25_074102_create_posts_table', 1),
(5, '2019_10_25_080225_create_tags_table', 1),
(6, '2019_10_25_081358_create_tag_post_table', 1),
(7, '2019_10_25_084034_create_medias_table', 1),
(8, '2019_10_25_084845_create_post_views_table', 1),
(9, '2019_10_25_091009_create_settings_table', 1),
(10, '2019_10_25_091853_create_support_emails_table', 1),
(11, '2019_10_25_092519_create_menu_tags_table', 1),
(12, '2019_10_25_105649_create_inquiries_table', 1),
(13, '2019_11_01_093335_create_roles_table', 1),
(14, '2019_11_01_093729_add_role_id_to_users_table', 1),
(15, '2019_11_07_021402_change_inquiry_work_status_inquiries_table', 2),
(16, '2019_11_07_151354_change_foreign_key_post_id_in_medias_table', 3);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `posts`
--

CREATE TABLE `posts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `content` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cover_image` bigint(20) UNSIGNED DEFAULT NULL,
  `cover_image_style` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cover_via_text` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cover_via_href` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `post_status` tinyint(1) NOT NULL DEFAULT 0,
  `user_id` bigint(20) UNSIGNED NOT NULL,
  `site_id` bigint(20) UNSIGNED NOT NULL,
  `html_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `html_meta_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `html_meta_keyword` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `canonical_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ogp_title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ogp_description` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `ogp_image_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `posts`
--

INSERT INTO `posts` (`id`, `title`, `content`, `cover_image`, `cover_image_style`, `cover_via_text`, `cover_via_href`, `post_status`, `user_id`, `site_id`, `html_title`, `html_meta_description`, `html_meta_keyword`, `canonical_url`, `ogp_title`, `ogp_description`, `ogp_image_url`, `created_at`, `updated_at`) VALUES
(1, 'Doloremque at quia a.', 'Doloribus deleniti quam aliquam neque eveniet in enim aliquam. Non laborum excepturi inventore eum. Quod aut explicabo nisi dolor. Exercitationem placeat officiis et et est expedita. Natus sit non est corporis ut. Doloribus et vitae quos fugit. Quasi recusandae et quia sapiente eos et optio commodi. Sed cupiditate est et cum. Atque facere optio eligendi eveniet voluptas. Ducimus quaerat laboriosam qui est. Laboriosam pariatur officiis aut est saepe id.', NULL, NULL, NULL, NULL, 1, 10, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-04 21:53:30', '2019-11-05 19:41:49'),
(2, 'Tenetur et officia atque officia asperiores.', 'Fugit nemo molestias omnis fugiat sed quos. Et qui in alias ea. Et et expedita quia est. Quasi vel nostrum nostrum ipsa atque saepe hic. Omnis est non unde beatae quo voluptatibus ut. Facere aut hic est dicta in qui et. Ab aut dicta nemo iste minima quaerat harum voluptatem. Delectus officiis autem voluptas. Adipisci ex incidunt est repellat. Possimus aut odio sed quisquam sint quod laudantium.', NULL, NULL, NULL, NULL, 1, 3, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-05 00:12:06', '2019-11-05 19:41:49'),
(3, 'Quod nulla totam sed.', 'Consequatur voluptatem libero consequatur distinctio et dolorem. Et sequi id non sit dolor dolorem exercitationem. Dolorem dolor illo enim. Repellat architecto ut voluptatem adipisci facere id. A nihil alias ducimus nostrum repudiandae doloribus. Rerum illo excepturi est consequatur. Et non dolore doloribus id tempore tempora qui repellat. Non et occaecati fuga dicta fugit quis.', NULL, NULL, NULL, NULL, 1, 3, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-05 00:12:06', '2019-11-05 19:41:49'),
(4, 'Laboriosam commodi est nam aspernatur soluta.', 'Autem saepe expedita aut ab numquam. Repellendus voluptas perspiciatis tempora quis et excepturi laudantium. Fuga aut et aut repudiandae. Quos natus expedita enim reprehenderit. Eius iure velit aperiam qui. Voluptatem aut provident beatae sed iure non ea. Aut sit molestiae laboriosam. Placeat voluptas voluptates quidem.', NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-05 00:12:07', '2019-11-05 19:41:50'),
(5, 'Temporibus neque beatae reprehenderit.', 'Veritatis voluptates et fugit illum. Ad neque et aut architecto. Nulla dolorum est laborum quia quia recusandae. Ab blanditiis similique qui quae aliquam. Quibusdam ut accusamus eum deserunt amet facilis quis. Rerum distinctio numquam nihil pariatur ex excepturi. Doloribus soluta dolor numquam harum alias. Aspernatur autem eos est magni laborum. Temporibus facere debitis iusto praesentium saepe quos mollitia. Quod perspiciatis dignissimos sed sint dolores perferendis numquam.', NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-05 00:12:07', '2019-11-05 19:41:50'),
(6, 'In molestiae aperiam eaque et eum eaque.', 'Recusandae ea eos illo illo laudantium. Ut et exercitationem placeat nemo. Dolore dolor nobis eos dolores quos doloribus quos. Dolor enim vitae doloremque blanditiis. Fugiat aliquam totam molestiae qui dolorem. Nulla saepe alias architecto eligendi eos. Molestias alias ipsam ipsa omnis. Est magni ut ut et quod libero.', NULL, NULL, NULL, NULL, 1, 8, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-05 00:12:07', '2019-11-05 19:41:50'),
(7, 'Dolores et at ut.', 'Itaque fuga voluptas dolorum aut ipsam sit. Ut et aut aut esse vel cum cupiditate. Porro rerum praesentium consequatur ipsa vero minima non nemo. Voluptatem et velit est. Ipsum id modi voluptatem pariatur perferendis iste facere. Nemo magnam aut eos quia nam eveniet. Consequatur fugiat possimus vel aspernatur porro. Error iure nemo molestias velit. Autem at hic totam non explicabo. Provident tenetur quis qui. Quia ipsa molestias quia fugiat sunt itaque.', NULL, NULL, NULL, NULL, 1, 6, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-05 00:12:07', '2019-11-05 19:41:50'),
(8, 'Voluptatem et et eum consectetur et sed.', 'Adipisci nesciunt consequuntur voluptates inventore ducimus soluta sit. Id id et provident. Impedit eos praesentium consequatur pariatur est cumque voluptatem temporibus. Saepe id nam nostrum modi iste laborum delectus. Animi incidunt voluptatum et. Porro necessitatibus dolor voluptatum. Velit reprehenderit dolore eligendi odio et cumque. Cumque aperiam iste sit modi voluptatem. Consectetur explicabo eius et corporis adipisci quia. Eum ut consectetur ut.', NULL, NULL, NULL, NULL, 1, 10, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-05 00:12:07', '2019-11-05 19:41:50'),
(9, 'Sed consectetur impedit nulla odit omnis in.', 'Corrupti quia temporibus sint laborum facilis recusandae ipsa. Distinctio quia amet voluptatibus dolores tenetur. Qui facere et sed animi. Quia facilis qui et fuga earum et. Et deserunt perspiciatis omnis sit dolor. Aliquid voluptatum explicabo illum porro. Quia est cum id culpa inventore dicta. Atque totam quam animi ut doloremque quibusdam. Iusto illum cum labore ut et recusandae. Temporibus velit recusandae maxime non veritatis libero. Dolorem aut quas non.', NULL, NULL, NULL, NULL, 1, 6, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-05 00:12:07', '2019-11-05 19:41:50'),
(10, 'Facilis incidunt occaecati iusto et temporibus provident laboriosam.', 'Minima natus natus optio voluptas. Nam laborum reiciendis debitis porro quas quisquam. Quia in perferendis hic porro aut quisquam qui. Blanditiis aut quis vero et sit aliquid consequatur in. Enim distinctio exercitationem tempore fuga repellendus. Dolor esse distinctio qui provident sit odio qui. Laborum odio placeat ipsam possimus eos porro. Quia vel ipsa esse. Ut est aut repellat et. Iste vel eum est voluptatibus. Sint iure voluptate quo sed est quasi non. Ut sapiente ea ipsam fugiat.', NULL, NULL, NULL, NULL, 1, 9, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-05 00:12:07', '2019-11-05 19:41:50'),
(11, 'Rem nemo iure aut vel.', 'Cupiditate ullam id qui voluptas illo sed optio. Necessitatibus qui quod iste. Est quos exercitationem minima optio mollitia quia. Tempora maiores amet excepturi eligendi. Nulla error quia vel et reiciendis consequatur voluptatibus. Inventore accusamus amet et quasi rerum quibusdam officia. Et deserunt molestiae minima id alias quia. Ipsam tenetur dicta quo earum dignissimos. Necessitatibus est molestias consequatur quo. Aut quas quo magni eaque sit non.', NULL, NULL, NULL, NULL, 1, 5, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-05 03:14:19', '2019-11-05 19:41:50'),
(12, 'Consequatur quasi est est in enim impedit.', 'Et quis excepturi et. Quas iste facilis est tenetur voluptates explicabo minima. Unde quos excepturi ad laudantium. Non officiis occaecati praesentium ea. Ipsam quae omnis autem est. Aut veniam magnam non voluptate expedita. Consequatur officia aut sequi rem voluptatem. Quos illum molestiae sapiente accusantium ut nulla et. Corrupti excepturi eum aperiam eum. Nihil officia nulla quia aliquam. Sapiente sed natus enim voluptatum impedit quam laborum eos. Ad et et ut voluptas non quo tempora.', NULL, NULL, NULL, NULL, 1, 10, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-05 03:14:19', '2019-11-05 19:41:51'),
(14, 'Iusto deleniti esse quia corporis.', 'Ut rerum ad possimus distinctio. Magnam iure dolor est fugit quasi culpa debitis. Totam magni dolor vero voluptatem iste nesciunt dignissimos. Officia rerum maiores quasi voluptates nihil non natus. Similique sint repudiandae sunt corporis consequatur ratione. Vel sunt ex earum. Aut nostrum esse architecto quis. Error repudiandae qui voluptas qui illum repellat voluptatem. Nam similique praesentium et minima atque. Ut qui rem distinctio quae sit vitae.', NULL, NULL, NULL, NULL, 1, 6, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-05 03:14:20', '2019-11-05 19:41:51'),
(15, 'Sequi est dolore dolor consequatur qui sint.', 'Nesciunt ad consequatur ullam repellendus tenetur voluptatem eum. Consequatur officia maiores earum omnis accusamus ut. Dolorem voluptas impedit et voluptatibus. Vero officiis voluptas sint quia voluptatem dicta autem. Consectetur et autem temporibus. Ea commodi quidem laboriosam at et. Quod doloribus nobis odit hic cupiditate. Est dolor voluptatum eligendi enim aut modi delectus architecto. Consequuntur officia quo recusandae non. Quae omnis et qui consectetur ullam quasi commodi.', NULL, NULL, NULL, NULL, 1, 5, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-05 03:14:20', '2019-11-05 19:41:51'),
(16, 'Delectus sint facilis accusamus omnis enim error eum.', 'Est quos sit non quia quia voluptas. Dolorem rerum corporis sunt ut eligendi. Laudantium esse voluptatibus natus enim quae beatae laborum. Nesciunt modi dolorum quis sed. Consequuntur suscipit magni optio possimus sit iste. Voluptatem aut reprehenderit commodi exercitationem.', NULL, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-05 03:14:20', '2019-11-05 19:41:51'),
(17, 'Qui odit temporibus voluptas sit officia similique voluptas.', 'Harum aperiam qui eligendi et ut blanditiis distinctio iure. Doloribus aliquam quasi sunt dolores. Dolorem rerum earum vero saepe officiis quaerat tenetur. At cum minima praesentium veniam consequuntur doloremque. Voluptate numquam praesentium consectetur occaecati dolores. Aliquam non doloremque consequatur id sint omnis ipsum. Culpa recusandae est iusto cumque voluptas.', NULL, NULL, NULL, NULL, 1, 5, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-05 03:14:20', '2019-11-05 19:41:51'),
(18, 'Reiciendis adipisci veniam voluptates ea molestiae illum porro.', 'Doloremque quae corporis dolor quia. Aut expedita fugit accusamus omnis. Laboriosam in sapiente sunt. Amet nulla eveniet qui veritatis. Eveniet cupiditate non laudantium ut eaque. Sit aliquam blanditiis sed modi perferendis quaerat deleniti. Est dolorem aut qui est laudantium non. Qui unde cum praesentium quas et dolorem. Laudantium nam deleniti natus. Quis ut et inventore corporis qui blanditiis. Optio vero numquam sit quaerat et velit. Praesentium voluptatem repellat nihil.', NULL, NULL, NULL, NULL, 1, 5, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-05 03:14:20', '2019-11-05 19:41:51'),
(19, 'Aut aliquid qui dolore dolor mollitia cumque quod.', 'Porro dignissimos laboriosam perferendis facere. Aut id eum error voluptatibus amet similique. Delectus eos accusamus ab. Dolores fugiat veniam exercitationem eligendi earum voluptatem. Ut architecto incidunt est voluptates adipisci eveniet. Similique quae consequatur dolor eos ut soluta. Quia corporis sit minus quis consequatur iure veritatis.', NULL, NULL, NULL, NULL, 1, 6, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-05 03:14:20', '2019-11-05 19:41:51'),
(20, 'Totam maiores perspiciatis vel dolores vel quo est nihil.', 'Alias ea repellat sunt sequi magnam cumque similique occaecati. Quam nobis nihil et rerum quisquam consequuntur sed. Et omnis possimus dolores adipisci atque optio. Asperiores optio natus ut laborum. Eum provident dolorum omnis voluptas nesciunt id. Molestiae culpa dolorem quas suscipit expedita incidunt. Ex est ab possimus inventore omnis sed inventore id. Non rerum laboriosam sit. Minus et incidunt sequi soluta distinctio delectus et.', NULL, NULL, NULL, NULL, 1, 5, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-05 03:14:20', '2019-11-05 19:41:51'),
(21, 'Tset Test', '<h4>Heading</h4><p><strong>Lorem Ipsum</strong> is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.</p><figure class=\"image\"><img src=\"http://local.adiva-world.com/storage/media/original/1573886693_mail.jpg\"><figcaption>asdasd</figcaption></figure><figure class=\"image image-style-side\"><img src=\"http://local.adiva-world.com/storage/media/original/1574044322_002-管理者TOP.png\"></figure>', 11, NULL, NULL, NULL, 1, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-14 08:37:11', '2019-11-18 02:52:51'),
(23, NULL, NULL, NULL, NULL, NULL, NULL, 0, 1, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '2019-11-17 20:08:38', '2019-11-17 20:08:38');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `post_views`
--

CREATE TABLE `post_views` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `session_id` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `ip` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `agent` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `roles`
--

CREATE TABLE `roles` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `slug` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `roles`
--

INSERT INTO `roles` (`id`, `slug`, `name`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'Admin', '2019-11-01 02:59:15', '2019-11-01 02:59:15'),
(2, 'writer', 'Writer', '2019-11-01 02:59:15', '2019-11-01 02:59:15');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `settings`
--

CREATE TABLE `settings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `key` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `display_name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `value` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `sites`
--

CREATE TABLE `sites` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `title` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `cover_image_url` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `user_id` bigint(20) UNSIGNED DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `sites`
--

INSERT INTO `sites` (`id`, `title`, `description`, `cover_image_url`, `user_id`, `created_at`, `updated_at`) VALUES
(1, 'Title', 'Description test', 'http://d1uzk9o9cg136f.cloudfront.net/resource/cover/10032.jpg', 1, '2019-11-01 02:59:15', '2019-11-04 20:18:49');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `support_emails`
--

CREATE TABLE `support_emails` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_approvide` tinyint(1) NOT NULL DEFAULT 0,
  `approvide_key` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `support_emails`
--

INSERT INTO `support_emails` (`id`, `email`, `is_approvide`, `approvide_key`, `created_at`, `updated_at`) VALUES
(1, 'abc@gmail.com', 1, 'xx2yj0v9H6xoLFX7wGnYVgHWiGPZRKp1PlkU2yjeFo9lRiQeX0', '2019-11-07 02:25:49', '2019-11-07 02:25:49');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tags`
--

CREATE TABLE `tags` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `order` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tags`
--

INSERT INTO `tags` (`id`, `name`, `order`, `created_at`, `updated_at`) VALUES
(2, 'cum', 1, '2019-11-04 21:29:43', '2019-11-06 02:55:01'),
(6, 'ea', 2, '2019-11-04 21:29:44', '2019-11-06 02:57:34'),
(7, 'voluptas', 3, '2019-11-04 21:29:44', '2019-11-06 02:57:34'),
(8, 'pariatur', 4, '2019-11-04 21:29:44', '2019-11-06 02:57:34'),
(10, 'accusamus', 5, '2019-11-04 21:29:44', '2019-11-06 02:57:34'),
(12, 'new tag', 6, '2019-11-06 02:57:13', '2019-11-06 02:57:34'),
(13, 'Test', 7, '2019-11-19 02:09:44', '2019-11-19 02:09:44'),
(14, 'ニュース', 8, '2019-11-19 02:35:45', '2019-11-19 02:35:45'),
(15, '知る', 9, '2019-11-19 02:35:53', '2019-11-19 02:35:53'),
(16, '走る', 10, '2019-11-19 02:36:05', '2019-11-19 02:36:05'),
(17, 'あそぶ', 11, '2019-11-19 02:36:15', '2019-11-19 02:36:15');

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `tag_post`
--

CREATE TABLE `tag_post` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tag_id` bigint(20) UNSIGNED NOT NULL,
  `post_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `tag_post`
--

INSERT INTO `tag_post` (`id`, `tag_id`, `post_id`, `created_at`, `updated_at`) VALUES
(32, 6, 3, NULL, NULL),
(33, 10, 4, NULL, NULL),
(35, 7, 6, NULL, NULL),
(36, 10, 7, NULL, NULL),
(43, 2, 14, NULL, NULL),
(44, 10, 15, NULL, NULL),
(47, 7, 18, NULL, NULL),
(49, 7, 20, NULL, NULL),
(50, 8, 21, NULL, NULL),
(51, 10, 21, NULL, NULL),
(52, 17, 21, NULL, NULL),
(53, 16, 21, NULL, NULL),
(54, 15, 21, NULL, NULL),
(55, 14, 21, NULL, NULL),
(56, 17, 20, NULL, NULL),
(57, 16, 20, NULL, NULL),
(58, 15, 20, NULL, NULL),
(59, 14, 20, NULL, NULL),
(60, 17, 19, NULL, NULL),
(61, 16, 19, NULL, NULL),
(62, 15, 19, NULL, NULL),
(63, 14, 19, NULL, NULL),
(64, 17, 18, NULL, NULL),
(65, 16, 18, NULL, NULL),
(66, 15, 18, NULL, NULL),
(67, 14, 18, NULL, NULL),
(68, 17, 17, NULL, NULL),
(69, 16, 17, NULL, NULL),
(70, 15, 17, NULL, NULL),
(71, 14, 17, NULL, NULL),
(72, 17, 16, NULL, NULL),
(73, 16, 16, NULL, NULL),
(74, 15, 16, NULL, NULL),
(75, 14, 16, NULL, NULL),
(76, 16, 15, NULL, NULL),
(77, 15, 15, NULL, NULL),
(78, 14, 15, NULL, NULL),
(79, 17, 15, NULL, NULL),
(80, 17, 14, NULL, NULL),
(81, 16, 14, NULL, NULL),
(82, 15, 14, NULL, NULL),
(83, 14, 14, NULL, NULL),
(84, 17, 12, NULL, NULL),
(85, 16, 12, NULL, NULL),
(86, 15, 12, NULL, NULL),
(87, 14, 12, NULL, NULL),
(88, 17, 11, NULL, NULL),
(89, 16, 11, NULL, NULL),
(90, 15, 11, NULL, NULL),
(91, 14, 11, NULL, NULL),
(92, 17, 10, NULL, NULL),
(93, 14, 10, NULL, NULL),
(94, 16, 10, NULL, NULL),
(95, 15, 10, NULL, NULL),
(96, 17, 9, NULL, NULL),
(97, 16, 9, NULL, NULL),
(98, 15, 9, NULL, NULL),
(99, 14, 9, NULL, NULL),
(100, 17, 8, NULL, NULL),
(101, 16, 8, NULL, NULL),
(102, 15, 8, NULL, NULL),
(103, 14, 8, NULL, NULL),
(104, 16, 7, NULL, NULL),
(105, 15, 7, NULL, NULL),
(106, 14, 7, NULL, NULL);

-- --------------------------------------------------------

--
-- Cấu trúc bảng cho bảng `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `avatar` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `writer_profile_html` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `facebook_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `twitter_id` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `receive_notification` tinyint(1) NOT NULL DEFAULT 0,
  `disable_article_publishing` tinyint(1) NOT NULL DEFAULT 0,
  `site_id` bigint(20) UNSIGNED DEFAULT NULL,
  `is_official_writer` tinyint(1) NOT NULL DEFAULT 0,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `role_id` bigint(20) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Đang đổ dữ liệu cho bảng `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `avatar`, `writer_profile_html`, `facebook_id`, `twitter_id`, `receive_notification`, `disable_article_publishing`, `site_id`, `is_official_writer`, `remember_token`, `created_at`, `updated_at`, `role_id`) VALUES
(1, 'Admin', 'admin@yopmail.com', '$2y$10$unFaz0l1srC5uqgtF0s8dOZe6dLfNSbEaaly9cVH8AUIK671Hr/iq', NULL, 'Test decscription html', NULL, NULL, 0, 0, 1, 0, NULL, '2019-11-01 02:59:15', '2019-11-05 03:39:21', 1),
(3, 'Mr. Dimitri Tremblay III', 'santa96@gmail.com', '$2y$10$XZOdwApH2OK8wDwcmn8aQe2vJUfzaT3/k7qCp715MMCuKIWLigkxG', NULL, NULL, NULL, NULL, 0, 0, 1, 0, NULL, '2019-11-05 03:39:21', '2019-11-05 03:39:21', 1),
(4, 'Else Kuhic', 'blair42@rutherford.com', '$2y$10$WdMPmQj3bD8yRvlL0ou7wOC1fisAMIKWOfRKse/OK/wwhcmk39oU2', NULL, NULL, NULL, NULL, 0, 0, 1, 0, NULL, '2019-11-05 03:39:21', '2019-11-05 03:39:21', 2),
(5, 'Dr. Armand Swaniawski', 'king.keshawn@wolff.net', '$2y$10$.5pf7noueSruiNlAzr1z5.mXTWIAR6f4/...4oRRzdokCvh0LwmjS', NULL, NULL, NULL, NULL, 0, 0, 1, 0, NULL, '2019-11-05 03:39:22', '2019-11-05 03:39:22', 1),
(6, 'Travon Torp', 'karolann.okon@block.com', '$2y$10$ls6/SPET.tltztl/q8/nZuxdUwdEjQoa9B0DjJ6QfVNs9DmPMfwKa', NULL, NULL, NULL, NULL, 0, 0, 1, 0, NULL, '2019-11-05 03:39:22', '2019-11-05 03:39:22', 1),
(8, 'Demetrius Kerluke PhD', 'reynold.bogan@hotmail.com', '$2y$10$b221qKWWNXscpP9kgdK4UOBGE3vNAa7aQ9nSAEu4TaxG8eHzj2t5u', NULL, NULL, NULL, NULL, 0, 0, 1, 0, NULL, '2019-11-05 03:39:22', '2019-11-05 03:39:22', 1),
(9, 'Ms. Joyce Eichmann', 'uthompson@hotmail.com', '$2y$10$Ku6l4w7iyJqXAKDJdLEu.u4hTkMFtsQvttIIG/X2HyEybqvZnjSUS', NULL, NULL, NULL, NULL, 0, 0, 1, 0, NULL, '2019-11-05 03:39:22', '2019-11-05 03:39:22', 1),
(10, 'Dr. Monte Bednar PhD', 'padberg.rebecca@hotmail.com', '$2y$10$GxKpS0oLUtTd8jQ9wuRqH.donU8pGtpzmO.6RxrEoOY2PSE6iU7hS', NULL, NULL, NULL, NULL, 0, 0, 1, 0, NULL, '2019-11-05 03:39:22', '2019-11-05 03:39:22', 1);

--
-- Chỉ mục cho các bảng đã đổ
--

--
-- Chỉ mục cho bảng `inquiries`
--
ALTER TABLE `inquiries`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `medias`
--
ALTER TABLE `medias`
  ADD PRIMARY KEY (`id`),
  ADD KEY `medias_post_id_foreign` (`post_id`);

--
-- Chỉ mục cho bảng `menu_tags`
--
ALTER TABLE `menu_tags`
  ADD PRIMARY KEY (`id`),
  ADD KEY `menu_tags_tag_id_foreign` (`tag_id`);

--
-- Chỉ mục cho bảng `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Chỉ mục cho bảng `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `posts_user_id_foreign` (`user_id`),
  ADD KEY `posts_site_id_foreign` (`site_id`);

--
-- Chỉ mục cho bảng `post_views`
--
ALTER TABLE `post_views`
  ADD PRIMARY KEY (`id`),
  ADD KEY `post_views_post_id_foreign` (`post_id`);

--
-- Chỉ mục cho bảng `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `roles_slug_unique` (`slug`);

--
-- Chỉ mục cho bảng `settings`
--
ALTER TABLE `settings`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `sites`
--
ALTER TABLE `sites`
  ADD PRIMARY KEY (`id`),
  ADD KEY `sites_user_id_foreign` (`user_id`);

--
-- Chỉ mục cho bảng `support_emails`
--
ALTER TABLE `support_emails`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `support_emails_email_unique` (`email`);

--
-- Chỉ mục cho bảng `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Chỉ mục cho bảng `tag_post`
--
ALTER TABLE `tag_post`
  ADD PRIMARY KEY (`id`),
  ADD KEY `tag_post_tag_id_foreign` (`tag_id`),
  ADD KEY `tag_post_post_id_foreign` (`post_id`);

--
-- Chỉ mục cho bảng `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`),
  ADD KEY `users_role_id_foreign` (`role_id`);

--
-- AUTO_INCREMENT cho các bảng đã đổ
--

--
-- AUTO_INCREMENT cho bảng `inquiries`
--
ALTER TABLE `inquiries`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `medias`
--
ALTER TABLE `medias`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT cho bảng `menu_tags`
--
ALTER TABLE `menu_tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT cho bảng `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT cho bảng `posts`
--
ALTER TABLE `posts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=24;

--
-- AUTO_INCREMENT cho bảng `post_views`
--
ALTER TABLE `post_views`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `roles`
--
ALTER TABLE `roles`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `settings`
--
ALTER TABLE `settings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT cho bảng `sites`
--
ALTER TABLE `sites`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT cho bảng `support_emails`
--
ALTER TABLE `support_emails`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT cho bảng `tags`
--
ALTER TABLE `tags`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT cho bảng `tag_post`
--
ALTER TABLE `tag_post`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=107;

--
-- AUTO_INCREMENT cho bảng `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- Các ràng buộc cho các bảng đã đổ
--

--
-- Các ràng buộc cho bảng `medias`
--
ALTER TABLE `medias`
  ADD CONSTRAINT `medias_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE SET NULL;

--
-- Các ràng buộc cho bảng `menu_tags`
--
ALTER TABLE `menu_tags`
  ADD CONSTRAINT `menu_tags_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `posts`
--
ALTER TABLE `posts`
  ADD CONSTRAINT `posts_site_id_foreign` FOREIGN KEY (`site_id`) REFERENCES `sites` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `posts_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `post_views`
--
ALTER TABLE `post_views`
  ADD CONSTRAINT `post_views_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `sites`
--
ALTER TABLE `sites`
  ADD CONSTRAINT `sites_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE SET NULL;

--
-- Các ràng buộc cho bảng `tag_post`
--
ALTER TABLE `tag_post`
  ADD CONSTRAINT `tag_post_post_id_foreign` FOREIGN KEY (`post_id`) REFERENCES `posts` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `tag_post_tag_id_foreign` FOREIGN KEY (`tag_id`) REFERENCES `tags` (`id`) ON DELETE CASCADE;

--
-- Các ràng buộc cho bảng `users`
--
ALTER TABLE `users`
  ADD CONSTRAINT `users_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE SET NULL;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
