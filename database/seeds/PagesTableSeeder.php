<?php

use Illuminate\Database\Seeder;
use App\Models\Page;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        if (Page::count() == 0) {
		        // Search page
		        Page::create([
		            'title' => '検索',
		            'slug' => 'search',
		            'user_id' => 1,
		            'status' => true
		        ]);
		        // Contact page
		        Page::create([
		            'title' => 'お問い合わせ',
		            'slug' => 'inquiry',
		            'user_id' => 1,
		            'status' => true
		        ]);
		        // Privacy-policy page
		        Page::create([
		            'title' => 'プライバシーポリシー',
		            'slug' => 'privacy-policy',
		            'user_id' => 1,
		            'status' => true
		        ]);
		        // Terms-of-service page
		        Page::create([
		            'title' => '利用規約',
		            'slug' => 'terms-of-service',
		            'user_id' => 1,
		            'status' => true
		        ]);
		        // Feature page
		        Page::create([
		            'title' => '特集一覧',
		            'slug' => 'feature',
		            'user_id' => 1,
		            'status' => true
		        ]);
		        // Series page
		        Page::create([
		            'title' => '連載一覧',
		            'slug' => 'series',
		            'user_id' => 1,
		            'status' => true
		        ]);
		      }
    }
}
