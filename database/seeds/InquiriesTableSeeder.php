<?php

use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use App\Models\Inquiry;

class InquiriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $faker = Faker::create();
    	for ($i=1; $i <= 10; $i++) { 
    		Inquiry::updateOrCreate(['id' => $i],
    			[
    				'email' => $faker->email,
    				'content' => $faker->text(500),
    				'work_status' => 'acknowledged',
    				'agent' => $faker->userAgent
    			]
    		);
    	}
    }
}
