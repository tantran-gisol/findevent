@if(isset($relatedPosts) && count($relatedPosts))
	<div class="related-article">
		<h3>
			<span>Bài viết liên quan<span>
		</h3>
		<ul class="row" style="padding: 0; margin: 1rem 0;">
			@foreach($relatedPosts as $relatedPost)
				<li class="related-article-item d-flex col-lg-6 col-md-6 col-12">
					<div class="related-article-item-img">
						<a href="{{ route('frontend.post.show', $relatedPost->slug) }}">
							@if($relatedPost->featureImage)
								<img src="{{ Helper::getMediaUrl($relatedPost->featureImage, 'thumbnail') }}" />
							@else
								<img src="{{ asset('image/img-slide-demo.jpg') }}" />
							@endif
						</a>
					</div>
					<div class="related-article-item-content">
						<a href="{{ route('frontend.post.show', $relatedPost->slug) }}">
							<div class="related-article-item-content-title">{{ $relatedPost->title }}</div>
							<div class="related-article-item-content-author">{{ $relatedPost->user->name }}</div>
						</a>
					</div>
				</li>
			@endforeach
		</ul>
	</div>
@endif