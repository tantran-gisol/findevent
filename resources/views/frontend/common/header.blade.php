<header id="header">
  <div class="container-fluid">
    <div id="logo" class="pull-left">
      <a href="{{ url('online') }}">
        <img class="logo" width="200" height="48" src="https://allevents.in/img/ae-logo-website.png" title="All events in City" alt="All events in City logo">
      </a>
    </div>

    <nav id="nav-menu-container">
      <ul class="nav-menu"> 
        <li class="menu-has-children"><i class="fa fa-plus" style="color:white;"></i><a href="">Create Event</a>
          <ul>
            <li><a href="">My Events</a></li>
            <li><a href="">Promote Events</a></li>
            <li><a href="">Sell Tickets Online</a></li>
            <li><a href="">Pricing Plans</a></li>
          </ul>
        </li>          
        <li>
          <a href="{{ url('signup') }}">Sign Up</a>
          
        </li>
        <li>
          <div class="input-group" style="margin:-5px;width:180px;">
            <input type="text" class="form-control" placeholder="Search Events">
            <div class="input-group-append">
              <button class="btn btn-secondary" type="button">
                <i class="fa fa-search"></i>
              </button>
            </div>
          </div>
        </li>
      </ul>
    </nav><!-- #nav-menu-container -->
  </div>
</header>