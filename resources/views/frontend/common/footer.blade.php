<footer class="footer">
  <h2 class="hide">Site Navigation</h2>
    <div class="home-footer1">   
      <div class="container">
        <div class="row" style="float: left;text-align: left;">
          <div class="span3 ft1span3"> 
            <h3 class="footer-head-title" style="margin-left: 10px;">Host Events</h3>
            <ul class="home-footer-links">
              <li><a href="" target="_blank" title="Publish Your Events">Publish Your Events</a></li>
              <li><a href="" title="Promote Your Events">Promote Your Events</a></li> 
              <li><a href="" title="Sell Tickets Online">Sell Tickets Online</a></li> 
              <li><a href="" target="_blank" title="Host Recorded Events">Host Recorded Events</a></li>
              <li><a href="" title="Event Manager App">Event Manager App</a></li>
            </ul>
          </div>
          <div class="span3 ft1span3"> 
            <h3 class="footer-head-title" style="margin-left: 10px;">Discover Events</h3>
            <ul class="home-footer-links">
              <li><a href="">Events for You</a></li>
              <li><a href="">Virtual Events</a></li>
              <li><a href="">Get Event Updates</a></li>
              <li><a href="">Event Discovery App</a></li>
            </ul>
          </div>
          <div class="span3 ft1span3"> 
            <h3 class="footer-head-title" style="margin-left: 10px;">Explore</h3>
            <ul class="home-footer-links">
              <li><a href="" title="Event Plugins for websites">Plugins</a></li>
              <li><a rel="nofollow" class="tltp" href="" title="All Events Logo Media Kit">Media Kit</a></li>
              <li><a rel="nofollow" target="_blank" class="tltp track" data-track="Footer|click|Affiliate-home-page" href="" title="Join AllEvents Affiliate Program">Affiliate Program</a></li>
              <li><a rel="nofollow" class="tltp" href="" title="Support Center">Support Center</a></li>
              <li><a class="tltp" href="" title="Blog ">Blog</a></li>
            </ul>
          </div>
         
          <div class="span3 ft1span3">
            <div class="mb10">
              <h3 class="footer-head-title">#StayHappening</h3>
              <div class="footer-mb-center" style=" font-size: 13px; text-transform: none; color: #999; ">Go-to destination for events for more than 6 million people across the world.
              </div>
            </div>
          </div>   
        </div>
      </div>
    </div>
    <div class="home-footer2 hidden-phone">
      <div class="container" style="border-top: 1px solid #2E363F; border-bottom: 1px solid #2E363F;padding: 10px 0">  
        <span class="footer-head-title" style="color: #efefef;text-transform: none;font-family: pn-semibold ;margin: 10px 0 10px 10px;float: left;">Country</span>
        <ul style="margin-bottom: 0">
          <li><a href="">Argentina</a></li>
          <li><a href="">Australia</a></li>
          <li><a href="">Brazil</a></li>
          <li><a href="">Belgium</a></li>
          <li><a href="">Canada</a></li>
          <li><a href="">France</a></li>
          <li><a href="">Germany</a></li>
          <li><a href="">India</a></li>
          <li><a href="">Ireland</a></li>
          <li><a href="">Italy</a></li>
          <li><a href="">Malaysia</a></li>
          <li><a href="">Mexico</a></li>
          <li><a href="">Netherlands</a></li>
          <li><a href="">South Africa</a></li>
          <li><a href="">Spain</a></li>
          <li><a href="">Turkey</a></li>
          <li><a href="">United Kingdom</a></li>
          <li><a href="">United States</a></li>
        </ul>
      </div>
    </div>
    <div class="custom-footer-section" style="margin: 20px 0 30px 0;">
      <div class="container">
        <div class="row">
          <div class="span12 footer-links-cont"> 
            <h3 class="footer-head-title custom-footer-head-title">Trending in Worldwide</h3>
            <ul class="footer-links gray-links custom-footer-ul">
              <li><a href="" title="Festivals In The United States">Festivals In The United States</a></li>
              <li><a href="" title="Christmas Lights 2021 Events In The United States">Christmas Lights 2021 Events In The United States</a></li>
              <li><a href="" title="Christmas Lights 2021 Events In The United Kingdom">Christmas Lights 2021 Events In The United Kingdom</a></li>
              <li><a href="" title="Christmas Markets 2021 Events In The United States">Christmas Markets 2021 Events In The United States</a></li>
              <li><a href="" title="New Year's Eve 2022 Parades, Fireworks & Parties In USA">New Year's Eve 2022 Parades, Fireworks & Parties In USA</a></li>
              <li><a href="" title="Christmas Markets 2021 Events In The United Kingdom">Christmas Markets 2021 Events In The United Kingdom</a></li>
              <li><a href="" title="Christmas Holiday Parades Events & Celebrations In USA">Christmas Holiday Parades Events & Celebrations In USA</a></li>
            </ul>
          </div>
        </div>
      </div>
    </div>          
          
    <div class="home-footer3">
      <div class="container">
        <ul style="margin: 0;margin-left: 10px;">
          <li><a href="" title="About Us and the All Events in City team">About</a></li>
          <li><a href="" title="Join the team | Careers at Allevents.in">Team</a></li>
          <li><a href="">Careers</a></li>
          <li><a href="" target="_blank" title="Frequently Asked Questions and Support">Support</a></li>
          <li><a rel="nofollow" href="" title="Terms & Conditions">Terms of Service</a></li>
          <li><a rel="nofollow" href="" title="Privacy">Privacy</a></li>
          <li><a rel="nofollow" href="" title="Contact Us">Contact Us</a></li>  
        </ul>
      </div>          
    </div>

    <div class="home-footer4">
      <div class="container">
        <div class="span6 cpy-right " style="margin: 10px;text-align: left;"><span>© Copyright 2021. All Rights Reserved</span>  | <a href="" target="_blank" style="display:inline-block;">Powered by Amitech.co</a></div>
        <p class="footer-social pull-right">
          <a class="btn btn-topbar tltp" title="Go to Homepage" data-original-title="Go to Homepage" href=""><i class="fas fa-home"></i></a>
          <a rel="nofollow noopener noreferrer" class="btn btn-topbar tltp" title="Like us Facebook" data-original-title="Like us Facebook" target="_blank" href=""><i class="fab fa-facebook-f"></i></a>
          <a rel="nofollow noopener noreferrer" class="btn btn-topbar tltp" title="Follow on Twitter" data-original-title="Follow on Twitter" target="_blank" href=""><i class="fab fa-twitter"></i></a>
          <a rel="nofollow noopener noreferrer" class="btn btn-topbar tltp" title="Follow us on Instagram" data-original-title="Follow us on Instagram" target="_blank" href=""><i class="fab fa-instagram"></i></a>
          <!--            <a rel="nofollow" rel="nofollow" class="btn btn-topbar tltp" title="Subscribe RSS Feeds" data-original-title="Subscribe RSS Feeds" href="https://allevents.in/Online/RSS"><i class="icon-rss"></i></a>
           -->
          <!-- <a rel="nofollow" id="back-to-top" class="btn btn-small btn-inverse tltp" title="Back to Top" data-original-title="Back to Top" href="#"><i class="icon-chevron-up icon-white"></i></a> -->
        </p>
      </div>
    </div>
</footer>