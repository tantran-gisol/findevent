@extends('frontend.layouts.app')
@section('content')
<div class="bluenavbar fixed minheight-5"></div>
<section id="intro">
  <div class="intro-container">
    <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="carousel-item active" style="height:250px;">
          <div class="carousel-background">
            <img src="{{ asset('image/banner1.jpg') }}">
          </div>
          <div class="carousel-container">
            <div class="carousel-content">
              <h2>Explore Events In United States </h2>
              <p>200M+ Events  |  30,000 Cities  |  4M People Exploring Events every month</p>
              <!-- <a href="#featured-services" class="btn-get-started scrollto">Get Started</a> -->
              <!-- <input class="span6 searchinput" id="home-search" type="text" value="Online" placeholder="Enter your city here"> -->
              <div class="main-search-input-wrap">
                <div class="main-search-input fl-wrap">
                  <div class="main-search-input-item">
                    <input type="text" value="" placeholder="All Events">
                  </div>
                  <button class="main-search-button">Explore</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

@endsection          