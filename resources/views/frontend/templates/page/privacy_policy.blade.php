@extends('frontend.layouts.app')
@section('content')
	<section class="main">

		<!-- main privacy policy page content -->

		<div class="main-privacy-policy-page">
			<div class="container">
				<div class="privacy-policy-page-header row">
					<div class="col-12">
						<h1>プライバシーポリシー</h1>
					</div>
				</div>
			
				<div class="line"></div>

				<div class="term-of-service-page row">
					
					<!-- left terms of service content -->

					<div class="privacy-policy-content col-lg-8 col-md-8 col-12">
						<div class="privacy-policy">
							<h3>第1章  個人情報の取り扱いについて</h3>

							<h4>1. はじめに</h4>

							<p>当社は、ユーザー様が本サービスを安心して利用いただけますように、個人情報（お名前、メールアドレス、およびアカウント、パスワードなど個人を識別する情報）を個人の重要な財産と認識し、本プライバシーポリシーに則り、ユーザー様から提供される個人情報を厳重かつ大切に管理いたします。</p>
						</div>

						<div class="privacy-policy">
							<h4>2. 基本方針</h4>

							<p>（1）個人情報の収集は、常に利用目的を明示し、本人の同意を得た上で行い、その利用目的のために必要な範囲を超えて個人情報を取り扱うことはありません。</p>

							<p>（2）収集した個人情報の取扱において、技術的・組織的に総合的なセキュリティ対策を講じ、適切な管理の下、個人情報を保存します。</p>

							<p>（3）個人情報の保護に関する法令及びその他の規範を遵守いたします。</p>
						</div>

						<div class="privacy-policy">
							<h4>3. 個人情報の取得</h4>

							<p>（1）本サービスのご登録手続きに際し、個人認証のため、個人情報の提供をお願いしています。広告プログラムなど一部のサービスでは、クレジットカードその他のお支払いに関するアカウント情報の提供をいただく可能性がありますが、これらの情報は安全なサーバー上で暗号化して管理いたします。</p>

							<p>（2）ユーザー様のアカウントに含まれる情報を第三者から取得した情報と統合し、ユーザー様の利便性の向上および本サービスの品質向上のために使用する場合もあります。予めご了承ください。</p>
						</div>
					</div>

					<!--x-- left terms of service content --x-->

					<!-- right category content -->

					@include('frontend.parts.right_sidebar')

					<!--x-- right category content --x-->

				</div>
			</div>
		</div>

		<!--x-- main privacy policy page content --x-->

	</section>
@endsection