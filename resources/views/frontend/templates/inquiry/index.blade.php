@extends('frontend.layouts.app')
@section('content')
	<section class="main">

		<!-- main privacy policy page content -->

		<div class="main-inquiry-page">
			<div class="container">
				<div class="inquiry-page-header row">
					<div class="col-12">
						<!-- <h1>Inquiry</h1> -->
						<h1>{{ $page->title }}</h1>
					</div>
				</div>
			
				<div class="line"></div>

				<div class="inquiry-page row">
					
					<!-- left inquiry content -->

					<div class="inquiry-content col-lg-8 col-md-8 col-12">
						<form class="inquiry-form" action="{{ route('frontend.inquiry.submit') }}" method="POST">
							{{ csrf_field() }}
							<label class="col-12">E-Mail</label>
							<input type="email" id="email" name="email" value="{{ old('email') }}" placeholder="your email" innerHTML>
							<div class="error_msg text-danger font-weight-bold" id="email_error" data-for="email">
								@if($errors->has('email'))
									{{ $errors->first('email') }}
								@endif
							</div>
							<label class="col-12">Message</label>
							<textarea type="text" id="message" name="message" placeholder="content of question" innerHTML>{{ old('message') }}</textarea>
							<div class="error_msg text-danger font-weight-bold" id="msg_error" data-for="message">
								@if($errors->has('message'))
									{{ $errors->first('message') }}
								@endif
							</div>
							<input type="submit" value="Send">
						</form>
					</div>

					<!--x-- left inquiry content --x-->

					<!-- right category content -->

					@include('frontend.parts.right_sidebar')

					<!--x-- right category content --x-->

				</div>
			</div>
		</div>

		<!--x-- main privacy policy page content --x-->

	</section>
@endsection