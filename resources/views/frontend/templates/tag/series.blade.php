@extends('frontend.layouts.app')
@section('content')
	<section class="main">

		<!-- main feature list page content -->

		<div class="main-series-list-page">
			<div class="container">
				
				<div class="series-list-page-header row">
					<div class="col-lg-6 col-md-6 col-12">
						<div class="series-list-page-header-img" style="background-image: url('{{ asset('image/img-1.jpg') }}');">
							
						</div>
					</div>
					<div class="col-lg-6 col-md-6 col-12">
						<h3>Tin tức</h3>
					</div>
				</div>

				<div class="line"></div>

				<div class="row">
					
					<!-- left series list page content -->

					<div class="left-series-list-page col-lg-8 col-md-8 col-12">
						<div class="series-list-page row">
							<div class="series-list-page-item col-lg-6 col-md-6 col-12">
								<a href="#">
									<div class="text-center">
										<label><i class="fas fa-tag"></i>Bạn có mơ thấy một chiếc xe máy điện?</label>
					
										<img class="w-100" src="{{ asset('image/img-slide-demo.jpg') }}">
									</div>
								</a>
							</div>

							<div class="series-list-page-item col-lg-6 col-md-6 col-12">
								<a href="#">
									<div class="text-center">
										<label><i class="fas fa-tag"></i>Bạn có mơ thấy một chiếc xe máy điện?</label>
					
										<img class="w-100" src="{{ asset('image/img-slide-demo.jpg') }}">
									</div>
								</a>
							</div>

							<div class="series-list-page-item col-lg-6 col-md-6 col-12">
								<a href="#">
									<div class="text-center">
										<label><i class="fas fa-tag"></i>Bạn có mơ thấy một chiếc xe máy điện?</label>
					
										<img class="w-100" src="{{ asset('image/img-slide-demo.jpg') }}">
									</div>
								</a>
							</div>

							<div class="series-list-page-item col-lg-6 col-md-6 col-12">
								<a href="#">
									<div class="text-center">
										<label><i class="fas fa-tag"></i>Bạn có mơ thấy một chiếc xe máy điện?</label>
					
										<img class="w-100" src="{{ asset('image/img-slide-demo.jpg') }}">
									</div>
								</a>
							</div>

							<div class="series-list-page-item col-lg-6 col-md-6 col-12">
								<a href="#">
									<div class="text-center">
										<label><i class="fas fa-tag"></i>Bạn có mơ thấy một chiếc xe máy điện?</label>
					
										<img class="w-100" src="{{ asset('image/img-slide-demo.jpg') }}">
									</div>
								</a>
							</div>

							<div class="series-list-page-item col-lg-6 col-md-6 col-12">
								<a href="#">
									<div class="text-center">
										<label><i class="fas fa-tag"></i>Bạn có mơ thấy một chiếc xe máy điện?</label>
					
										<img class="w-100" src="{{ asset('image/img-slide-demo.jpg') }}">
									</div>
								</a>
							</div>
						</div>
					</div>

					<!--x-- left feature list page content --x-->

					<!-- right category content -->

					@include('frontend.parts.right_sidebar')

					<!--x-- right category content --x-->

				</div>
			</div>
		</div>

		<!--x-- main feature list page content --x-->

	</section>
@endsection