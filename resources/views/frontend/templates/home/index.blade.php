@extends('frontend.layouts.app')
@section('content')
<div class="bluenavbar fixed minheight-5"></div>
<section id="intro">
  <div class="intro-container">
    <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="carousel-item active" style="height:250px;">
          <div class="carousel-background">
            <img src="{{ asset('image/banner1.jpg') }}">
          </div>
          <div class="carousel-container">
            <div class="carousel-content">
              <h2>Discover Events Happening in Your City</h2>
              <p>200M+ Events  |  30,000 Cities  |  4M People Exploring Events every month</p>
              <!-- <a href="#featured-services" class="btn-get-started scrollto">Get Started</a> -->
              <div class="main-search-input-wrap">
                <div class="main-search-input fl-wrap">
                  <div class="main-search-input-item">
                    <div class="input-group">
                      <div class="input-group-prepend">
                        <input type="text" class="form-control">
                      </div>
                      <input type="text" class="form-control">
                      <input type="text" class="form-control">
                    </div>
                  </div>
                  <button class="main-search-button">Explore</button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<main id="main">
  <section id="services">
    <div class="container">
      <h3 class="section-header">Discover Best Of Online Events</h3>
      <div class="new-content row">
        @if((new \Jenssegers\Agent\Agent())->isDesktop())
          @php
            $posts = $postsPC;
          @endphp
          @else
          @php
            $posts = $postsSP;
          @endphp
        @endif
        @if(count($posts))
          @foreach($posts as $post)
            <div class="card-content col-lg-4 col-6">
              <div class="card-new-content">
                <a href="{{ route('frontend.post.show', $post->slug) }}">
                  <div class="card-new-content-img img-wrap" style="background: url('{{ ($post->featureImage) ? Helper::getMediaUrl($post->featureImage, 'medium') : Helper::getDefaultCover($post) }}');">
                  @if($post->featureImage)
                    <img src="{{ Helper::getMediaUrl($post->featureImage, 'medium') }}" />
                  @else
                    <img src="{{ Helper::getDefaultCover($post) }}">
                  @endif
                  </div>
                </a>

                <div class="post-man d-flex justify-content-between">
                  <label class="posted-date d-none d-md-block">{{ date('Y-m-d', strtotime($post->created_at)) }}</label>
                  <label class="card-category"><a href="{{ route('frontend.user.show', $post->user) }}">{{ $post->user->name }}</a></label>
                </div>
                <div class="artical-title">
                  <a href="{{ route('frontend.post.show', $post->slug) }}">{{ $post->title }}</a>
                </div>
              </div>
            </div>
          @endforeach
        @endif
      </div>
      <div class="row">
        <!-- pagination -->
        {{ $posts->appends(request()->query())->links('frontend.parts.pagination') }}
        <!--x-- pagination -->
      <!-- </div> -->
      <!---x--- card new content ---x--->
      </div>
    </div> 
  </section>
  <section id="clients" class="wow fadeInUp">
    <div class="container">
      <h3 class="section-header">Things To Do At Home</h3>
      <div class="row pop-cities">
        <div class="span2 ranm" style="background: url(//cdn2.allevents.in/transup/9e/9f5022cc7242e995fc8373bf6d51c7/online_health_and_wellness_yoga_allevents_in.jpg); background-size: cover;">
          <a href="" title="Online Health & Wellness Events">
            <div class="overlay">
              <p>Health & Wellness</p>
            </div>
          </a>
        </div>
        <div class="span2 ranm" style="background: url(////cdn2.allevents.in/transup/64/565a991e8347a7bf7ff5d51c603fa6/online_games_allevents_in.jpg); background-size: cover;">
          <a href="" title="Online Health & Wellness Events">
            <div class="overlay">
              <p>Games</p>
            </div>
          </a>
        </div>
        <div class="span2 ranm" style="background: url(//cdn-az.allevents.in/banners/c1d373a7f659665c228acd1e3e7398a9); background-size: cover;">
          <a href="" title="Online Health & Wellness Events">
            <div class="overlay">
              <p>Meetups</p>
            </div>
          </a>
        </div>
        <div class="span2 ranm" style="background: url(//cdn-az.allevents.in/banners/75547550c0e98622703cd444203cef3b); background-size: cover;">
          <a href="" title="Online Health & Wellness Events">
            <div class="overlay">
              <p>Music</p>
            </div>
          </a>
        </div>
      </div>
      <div class="row pop-cities">
        <div class="span2 ranm" style="background: url(//cdn2.allevents.in/transup/86/aba165bf804c4a9d9f1e554c526e31/online_art_events_allevents_in.jpg); background-size: cover;">
          <a href="" title="Online Health & Wellness Events">
            <div class="overlay">
              <p>Art</p>
            </div>
          </a>
        </div>
        <div class="span2 ranm" style="background: url(//cdn-az.allevents.in/banners/344231a933cb65fd654990cbd7edbc70); background-size: cover;">
          <a href="" title="Online Health & Wellness Events">
            <div class="overlay">
              <p>Food & Drinks</p>
            </div>
          </a>
        </div>
        <div class="span2 ranm" style="background: url(//cdn-az.allevents.in/banners/473e2e44128c7b0293eb02d7d5609edc); background-size: cover;">
          <a href="" title="Online Health & Wellness Events">
            <div class="overlay">
              <p>Business</p>
            </div>
          </a>
        </div>
        <div class="span2 ranm" style="background: url(//cdn-az.allevents.in/banners/f31adad8afa731706b70d8c28c95dccc); background-size: cover;">
          <a href="" title="Online Health & Wellness Events">
            <div class="overlay">
              <p>Sports</p>
            </div>
          </a>
        </div>
      </div>
      <div style="text-align: center;">
        <a class="btn btn-large tltp" href="" style="color: #555" data-original-title="View all Online Events">View all Online Events
          <i class="fas fa-angle-right"></i>
        </a> 
      </div>      
    </div>  
  </section>
  <div style="border-bottom: 1px solid #ddd;padding: 30px 0;padding-bottom: 0;"></div>
  <section id="clients" class="wow fadeInUp">
    <div class="container">
      <h3 class="section-header">Explore Events across the World</h3>
      <div class="row pop-cities">
        <div class="span2 ranm" style="background: url(//cdn-az.allevents.in/banners/9f04adcf3ae5978c1f180aaaac5e5e63); background-size: cover;">
          <a href="" title="Online Health & Wellness Events">
            <div class="overlay">
              <p>Ahmedabad Events</p>
            </div>
          </a>
        </div>
        <div class="span2 ranm" style="background: url(//cdn-az.allevents.in/banners/e809a6763d7062670dc7b31dc57a3378); background-size: cover;">
          <a href="" title="Online Health & Wellness Events">
            <div class="overlay">
              <p>New York Events</p>
            </div>
          </a>
        </div>
        <div class="span2 ranm" style="background: url(//cdn-az.allevents.in/banners/b096e51554afd18e182fe70fa1737be2); background-size: cover;">
          <a href="" title="Online Health & Wellness Events">
            <div class="overlay">
              <p>Toronto Events</p>
            </div>
          </a>
        </div>
        <div class="span2 ranm" style="background: url(//cdn-az.allevents.in/banners/8532110f362d05a7b3b293b5649ef80c); background-size: cover;">
          <a href="" title="Online Health & Wellness Events">
            <div class="overlay">
              <p>Dublin Events</p>
            </div>
          </a>
        </div>
      </div>
      <div class="row pop-cities">
        <div class="span2 ranm" style="background: url(//cdn-az.allevents.in/banners/68c618eae303fa588192c458218b793e); background-size: cover;">
          <a href="" title="Online Health & Wellness Events">
            <div class="overlay">
              <p>Sydney Events</p>
            </div>
          </a>
        </div>
        <div class="span2 ranm" style="background: url(//cdn-az.allevents.in/banners/8c077d8281b4ddc526a754079d2aab66); background-size: cover;">
          <a href="" title="Online Health & Wellness Events">
            <div class="overlay">
              <p>London Events</p>
            </div>
          </a>
        </div>
        <div class="span2 ranm" style="background: url(//cdn-az.allevents.in/banners/1420064cb48220d10b2daff70f87c634); background-size: cover;">
          <a href="" title="Online Health & Wellness Events">
            <div class="overlay">
              <p>Milan Events</p>
            </div>
          </a>
        </div>
        <div class="span2 ranm" style="background: url(//cdn-az.allevents.in/banners/00f79a1cc1ced9c3b562a76652faf8a9); background-size: cover;">
          <a href="" title="Online Health & Wellness Events">
            <div class="overlay">
              <p>Berlin Events</p>
            </div>
          </a>
        </div>
      </div>
      <div class="row-fluid count-flag"style="display: block;">
        <div class="container-fluid">
          <div class="flags-bottom">
            <div class="cont container"> 
              <ul class="flag-list">
                <li class="flag">
                  <a href="">
                    <a href="{{ url('location') }}"><img src="//cdn5.allevents.in/images/flags/32/US.png" alt="United States"></a>
                  </a>
                  <div class="pop-cont">
                    <div class="pop-t">
                        <img src="//cdn5.allevents.in/images/flags/32/US.png" alt="United States">
                        <span class="ml5">United States</span>
                    </div>
                    <div class="pop-c">
                      <ul class="city-list">
                        <li><a href="">New York</a></li>
                        <li><a href="">Florida</a></li>
                        <li><a href="">Michigan</a></li>
                        <li><a href="">Ohio</a></li>
                        <li><a href="">Chicago</a></li>
                        <li><a href="">Los Angeles</a></li>
                        <li><a href="">Maryland</a></li>
                        <li><a href="">Las Vegas</a></li>
                        <li><a href="">Indiana</a></li>
                        <li><a href="">Washington</a></li>
                        <li><a href="">San Francisco</a></li>
                        <li><a href="">Tennessee</a></li>
                        <li><a href="">Atlanta</a></li>
                        <li><a href="" class="more-city">more</a></li>
                      </ul>
                      <div class="hidden-div"></div>
                    </div>
                  </div>
                </li>
                <li class="flag">
                  <a href="">
                    <img src="//cdn5.allevents.in/images/flags/32/CA.png" alt="United States">
                  </a>
                  <div class="pop-cont">
                    <div class="pop-t">
                        <img src="//cdn5.allevents.in/images/flags/32/CA.png" alt="United States">
                        <span class="ml5">Canada</span>
                    </div>
                    <div class="pop-c">
                      <ul class="city-list">
                        <li><a href="">New York</a></li>
                        <li><a href="">Florida</a></li>
                        <li><a href="">Michigan</a></li>
                        <li><a href="">Ohio</a></li>
                        <li><a href="">Chicago</a></li>
                        <li><a href="">Los Angeles</a></li>
                        <li><a href="">Maryland</a></li>
                        <li><a href="">Las Vegas</a></li>
                        <li><a href="">Indiana</a></li>
                        <li><a href="">Washington</a></li>
                        <li><a href="">San Francisco</a></li>
                        <li><a href="">Tennessee</a></li>
                        <li><a href="">Atlanta</a></li>
                        <li><a href="" class="more-city">more</a></li>
                      </ul>
                      <div class="hidden-div"></div>
                    </div>
                  </div>
                </li>
                <li class="flag">
                  <a href=""><img src="//cdn5.allevents.in/images/flags/32/FR.png" alt="United States"></a>
                  <div class="pop-cont">
                    <div class="pop-t">
                        <img src="//cdn5.allevents.in/images/flags/32/FR.png" alt="United States">
                        <span class="ml5">France</span>
                    </div>
                    <div class="pop-c">
                      <ul class="city-list">
                        <li><a href="">New York</a></li>
                        <li><a href="">Florida</a></li>
                        <li><a href="">Michigan</a></li>
                        <li><a href="">Ohio</a></li>
                        <li><a href="">Chicago</a></li>
                        <li><a href="">Los Angeles</a></li>
                        <li><a href="">Maryland</a></li>
                        <li><a href="">Las Vegas</a></li>
                        <li><a href="">Indiana</a></li>
                        <li><a href="">Washington</a></li>
                        <li><a href="">San Francisco</a></li>
                        <li><a href="">Tennessee</a></li>
                        <li><a href="">Atlanta</a></li>
                        <li><a href="" class="more-city">more</a></li>
                      </ul>
                      <div class="hidden-div"></div>
                    </div>
                  </div>
                </li>
                <li class="flag">
                  <a href=""><img src="//cdn5.allevents.in/images/flags/32/DE.png" alt="United States"></a>
                  <div class="pop-cont">
                    <div class="pop-t">
                        <img src="//cdn5.allevents.in/images/flags/32/DE.png" alt="United States">
                        <span class="ml5">United States</span>
                    </div>
                    <div class="pop-c">
                      <ul class="city-list">
                        <li><a href="">New York</a></li>
                        <li><a href="">Florida</a></li>
                        <li><a href="">Michigan</a></li>
                        <li><a href="">Ohio</a></li>
                        <li><a href="">Chicago</a></li>
                        <li><a href="">Los Angeles</a></li>
                        <li><a href="">Maryland</a></li>
                        <li><a href="">Las Vegas</a></li>
                        <li><a href="">Indiana</a></li>
                        <li><a href="">Washington</a></li>
                        <li><a href="">San Francisco</a></li>
                        <li><a href="">Tennessee</a></li>
                        <li><a href="">Atlanta</a></li>
                        <li><a href="" class="more-city">more</a></li>
                      </ul>
                      <div class="hidden-div"></div>
                    </div>
                  </div>
                </li>
                <li class="flag">
                  <a href=""><img src="//cdn5.allevents.in/images/flags/32/BR.png" alt="United States"></a>
                  <div class="pop-cont">
                    <div class="pop-t">
                        <img src="//cdn5.allevents.in/images/flags/32/BR.png" alt="United States">
                        <span class="ml5">United States</span>
                    </div>
                    <div class="pop-c">
                      <ul class="city-list">
                        <li><a href="">New York</a></li>
                        <li><a href="">Florida</a></li>
                        <li><a href="">Michigan</a></li>
                        <li><a href="">Ohio</a></li>
                        <li><a href="">Chicago</a></li>
                        <li><a href="">Los Angeles</a></li>
                        <li><a href="">Maryland</a></li>
                        <li><a href="">Las Vegas</a></li>
                        <li><a href="">Indiana</a></li>
                        <li><a href="">Washington</a></li>
                        <li><a href="">San Francisco</a></li>
                        <li><a href="">Tennessee</a></li>
                        <li><a href="">Atlanta</a></li>
                        <li><a href="" class="more-city">more</a></li>
                      </ul>
                      <div class="hidden-div"></div>
                    </div>
                  </div>
                </li>
                <li class="flag">
                  <a href=""><img src="//cdn5.allevents.in/images/flags/32/NL.png" alt="United States"></a>
                  <div class="pop-cont">
                    <div class="pop-t">
                        <img src="//cdn5.allevents.in/images/flags/32/NL.png" alt="United States">
                        <span class="ml5">United States</span>
                    </div>
                    <div class="pop-c">
                      <ul class="city-list">
                        <li><a href="">New York</a></li>
                        <li><a href="">Florida</a></li>
                        <li><a href="">Michigan</a></li>
                        <li><a href="">Ohio</a></li>
                        <li><a href="">Chicago</a></li>
                        <li><a href="">Los Angeles</a></li>
                        <li><a href="">Maryland</a></li>
                        <li><a href="">Las Vegas</a></li>
                        <li><a href="">Indiana</a></li>
                        <li><a href="">Washington</a></li>
                        <li><a href="">San Francisco</a></li>
                        <li><a href="">Tennessee</a></li>
                        <li><a href="">Atlanta</a></li>
                        <li><a href="" class="more-city">more</a></li>>
                      </ul>
                      <div class="hidden-div"></div>
                    </div>
                  </div>
                </li>
                <li class="flag">
                  <a href="">
                    <img src="//cdn5.allevents.in/images/flags/32/AU.png" alt="United States">
                  </a>
                  <div class="pop-cont">
                    <div class="pop-t">
                        <img src="//cdn5.allevents.in/images/flags/32/AU.png" alt="United States">
                        <span class="ml5">United States</span>
                    </div>
                    <div class="pop-c">
                      <ul class="city-list">
                        <li><a href="">New York</a></li>
                        <li><a href="">Florida</a></li>
                        <li><a href="">Michigan</a></li>
                        <li><a href="">Ohio</a></li>
                        <li><a href="">Chicago</a></li>
                        <li><a href="">Los Angeles</a></li>
                        <li><a href="">Maryland</a></li>
                        <li><a href="">Las Vegas</a></li>
                        <li><a href="">Indiana</a></li>
                        <li><a href="">Washington</a></li>
                        <li><a href="">San Francisco</a></li>
                        <li><a href="">Tennessee</a></li>
                        <li><a href="">Atlanta</a></li>
                        <li><a href="" class="more-city">more</a></li>
                      </ul>
                      <div class="hidden-div"></div>
                    </div>
                  </div>
                </li>
                <li class="flag">
                  <a href=""><img src="//cdn5.allevents.in/images/flags/32/ES.png" alt="United States"></a>
                  <div class="pop-cont">
                    <div class="pop-t">
                        <img src="//cdn5.allevents.in/images/flags/32/ES.png" alt="United States">
                        <span class="ml5">United States</span>
                    </div>
                    <div class="pop-c">
                      <ul class="city-list">
                        <li><a href="">New York</a></li>
                        <li><a href="">Florida</a></li>
                        <li><a href="">Michigan</a></li>
                        <li><a href="">Ohio</a></li>
                        <li><a href="">Chicago</a></li>
                        <li><a href="">Los Angeles</a></li>
                        <li><a href="">Maryland</a></li>
                        <li><a href="">Las Vegas</a></li>
                        <li><a href="">Indiana</a></li>
                        <li><a href="">Washington</a></li>
                        <li><a href="">San Francisco</a></li>
                        <li><a href="">Tennessee</a></li>
                        <li><a href="">Atlanta</a></li>
                        <li><a href="" class="more-city">more</a></li>
                      </ul>
                      <div class="hidden-div"></div>
                    </div>
                  </div>
                </li>
                <li class="flag">
                  <a href=""><img src="//cdn5.allevents.in/images/flags/32/FI.png" alt="United States"></a>
                  <div class="pop-cont">
                    <div class="pop-t">
                        <img src="//cdn5.allevents.in/images/flags/32/FI.png" alt="United States">
                        <span class="ml5">United States</span>
                    </div>
                    <div class="pop-c">
                      <ul class="city-list">
                        <li><a href="">New York</a></li>
                        <li><a href="">Florida</a></li>
                        <li><a href="">Michigan</a></li>
                        <li><a href="">Ohio</a></li>
                        <li><a href="">Chicago</a></li>
                        <li><a href="">Los Angeles</a></li>
                        <li><a href="">Maryland</a></li>
                        <li><a href="">Las Vegas</a></li>
                        <li><a href="">Indiana</a></li>
                        <li><a href="">Washington</a></li>
                        <li><a href="">San Francisco</a></li>
                        <li><a href="">Tennessee</a></li>
                        <li><a href="">Atlanta</a></li>
                        <li><a href="" class="more-city">more</a></li>
                      </ul>
                      <div class="hidden-div"></div>
                    </div>
                  </div>
                </li>
                <li class="flag">
                  <a href=""><img src="//cdn5.allevents.in/images/flags/32/AT.png" alt="United States"></a>
                  <div class="pop-cont">
                    <div class="pop-t">
                        <img src="//cdn5.allevents.in/images/flags/32/AT.png" alt="United States">
                        <span class="ml5">United States</span>
                    </div>
                    <div class="pop-c">
                      <ul class="city-list">
                        <li><a href="">New York</a></li>
                        <li><a href="">Florida</a></li>
                        <li><a href="">Michigan</a></li>
                        <li><a href="">Ohio</a></li>
                        <li><a href="">Chicago</a></li>
                        <li><a href="">Los Angeles</a></li>
                        <li><a href="">Maryland</a></li>
                        <li><a href="">Las Vegas</a></li>
                        <li><a href="">Indiana</a></li>
                        <li><a href="">Washington</a></li>
                        <li><a href="">San Francisco</a></li>
                        <li><a href="">Tennessee</a></li>
                        <li><a href="">Atlanta</a></li>
                        <li><a href="" class="more-city">more</a></li>
                      </ul>
                      <div class="hidden-div"></div>
                    </div>
                  </div>
                </li>
                <li class="flag">
                  <a href=""><img src="//cdn5.allevents.in/images/flags/32/ZA.png" alt="United States"></a>
                  <div class="pop-cont">
                    <div class="pop-t">
                        <img src="//cdn5.allevents.in/images/flags/32/ZA.png" alt="United States">
                        <span class="ml5">United States</span>
                    </div>
                    <div class="pop-c">
                      <ul class="city-list">
                        <li><a href="">New York</a></li>
                        <li><a href="">Florida</a></li>
                        <li><a href="">Michigan</a></li>
                        <li><a href="">Ohio</a></li>
                        <li><a href="">Chicago</a></li>
                        <li><a href="">Los Angeles</a></li>
                        <li><a href="">Maryland</a></li>
                        <li><a href="">Las Vegas</a></li>
                        <li><a href="">Indiana</a></li>
                        <li><a href="">Washington</a></li>
                        <li><a href="">San Francisco</a></li>
                        <li><a href="">Tennessee</a></li>
                        <li><a href="">Atlanta</a></li>
                        <li><a href="" class="more-city">more</a></li>
                      </ul>
                      <div class="hidden-div"></div>
                    </div>
                  </div>
                </li>
                <li class="flag">
                  <a href=""><img src="//cdn5.allevents.in/images/flags/32/IN.png" alt="United States"></a>
                  <div class="pop-cont">
                    <div class="pop-t">
                        <img src="//cdn5.allevents.in/images/flags/32/IN.png" alt="United States">
                        <span class="ml5">United States</span>
                    </div>
                    <div class="pop-c">
                      <ul class="city-list">
                        <li><a href="">New York</a></li>
                        <li><a href="">Florida</a></li>
                        <li><a href="">Michigan</a></li>
                        <li><a href="">Ohio</a></li>
                        <li><a href="">Chicago</a></li>
                        <li><a href="">Los Angeles</a></li>
                        <li><a href="">Maryland</a></li>
                        <li><a href="">Las Vegas</a></li>
                        <li><a href="">Indiana</a></li>
                        <li><a href="">Washington</a></li>
                        <li><a href="">San Francisco</a></li>
                        <li><a href="">Tennessee</a></li>
                        <li><a href="">Atlanta</a></li>
                        <li><a href="" class="more-city">more</a></li>
                      </ul>
                      <div class="hidden-div"></div>
                    </div>
                  </div>
                </li>
                <li class="flag">
                  <a href=""><img src="//cdn5.allevents.in/images/flags/32/PL.png" alt="United States"></a>
                  <div class="pop-cont">
                    <div class="pop-t">
                        <img src="//cdn5.allevents.in/images/flags/32/PL.png" alt="United States">
                        <span class="ml5">United States</span>
                    </div>
                    <div class="pop-c">
                      <ul class="city-list">
                        <li><a href="">New York</a></li>
                        <li><a href="">Florida</a></li>
                        <li><a href="">Michigan</a></li>
                        <li><a href="">Ohio</a></li>
                        <li><a href="">Chicago</a></li>
                        <li><a href="">Los Angeles</a></li>
                        <li><a href="">Maryland</a></li>
                        <li><a href="">Las Vegas</a></li>
                        <li><a href="">Indiana</a></li>
                        <li><a href="">Washington</a></li>
                        <li><a href="">San Francisco</a></li>
                        <li><a href="">Tennessee</a></li>
                        <li><a href="">Atlanta</a></li>
                        <li><a href="" class="more-city">more</a></li>
                      </ul>
                      <div class="hidden-div"></div>
                    </div>
                  </div>
                </li>
                <li class="flag">
                  <a href=""><img src="//cdn5.allevents.in/images/flags/32/NZ.png" alt="United States"></a>
                  <div class="pop-cont">
                    <div class="pop-t">
                        <img src="//cdn5.allevents.in/images/flags/32/NZ.png" alt="United States">
                        <span class="ml5">United States</span>
                    </div>
                    <div class="pop-c">
                      <ul class="city-list">
                        <li><a href="">New York</a></li>
                        <li><a href="">Florida</a></li>
                        <li><a href="">Michigan</a></li>
                        <li><a href="">Ohio</a></li>
                        <li><a href="">Chicago</a></li>
                        <li><a href="">Los Angeles</a></li>
                        <li><a href="">Maryland</a></li>
                        <li><a href="">Las Vegas</a></li>
                        <li><a href="">Indiana</a></li>
                        <li><a href="">Washington</a></li>
                        <li><a href="">San Francisco</a></li>
                        <li><a href="">Tennessee</a></li>
                        <li><a href="">Atlanta</a></li>
                        <li><a href="" class="more-city">more</a></li>
                      </ul>
                      <div class="hidden-div"></div>
                    </div>
                  </div>
                </li>
                <li class="flag">
                  <a href=""><img src="//cdn5.allevents.in/images/flags/32/CH.png" alt="United States"></a>
                  <div class="pop-cont">
                    <div class="pop-t">
                        <img src="//cdn5.allevents.in/images/flags/32/CH.png" alt="United States">
                        <span class="ml5">United States</span>
                    </div>
                    <div class="pop-c">
                      <ul class="city-list">
                        <li><a href="">New York</a></li>
                        <li><a href="">Florida</a></li>
                        <li><a href="">Michigan</a></li>
                        <li><a href="">Ohio</a></li>
                        <li><a href="">Chicago</a></li>
                        <li><a href="">Los Angeles</a></li>
                        <li><a href="">Maryland</a></li>
                        <li><a href="">Las Vegas</a></li>
                        <li><a href="">Indiana</a></li>
                        <li><a href="">Washington</a></li>
                        <li><a href="">San Francisco</a></li>
                        <li><a href="">Tennessee</a></li>
                        <li><a href="">Atlanta</a></li>
                        <li><a href="" class="more-city">more</a></li>
                      </ul>
                      <div class="hidden-div"></div>
                    </div>
                  </div>
                </li>
                <li class="flag">
                  <a href=""><img src="//cdn5.allevents.in/images/flags/32/CH.png" alt="United States"></a>
                  <div class="pop-cont">
                    <div class="pop-t">
                        <img src="//cdn5.allevents.in/images/flags/32/CH.png" alt="United States">
                        <span class="ml5">United States</span>
                    </div>
                    <div class="pop-c">
                      <ul class="city-list">
                        <li><a href="">New York</a></li>
                        <li><a href="">Florida</a></li>
                        <li><a href="">Michigan</a></li>
                        <li><a href="">Ohio</a></li>
                        <li><a href="">Chicago</a></li>
                        <li><a href="">Los Angeles</a></li>
                        <li><a href="">Maryland</a></li>
                        <li><a href="">Las Vegas</a></li>
                        <li><a href="">Indiana</a></li>
                        <li><a href="">Washington</a></li>
                        <li><a href="">San Francisco</a></li>
                        <li><a href="">Tennessee</a></li>
                        <li><a href="">Atlanta</a></li>
                        <li><a href="" class="more-city">more</a></li>
                      </ul>
                      <div class="hidden-div"></div>
                    </div>
                  </div>
                </li>
                <li class="flag">
                  <a href=""><img src="//cdn5.allevents.in/images/flags/32/NO.png" alt="United States"></a>
                  <div class="pop-cont">
                    <div class="pop-t">
                        <img src="//cdn5.allevents.in/images/flags/32/NO.png" alt="United States">
                        <span class="ml5">United States</span>
                    </div>
                    <div class="pop-c">
                      <ul class="city-list">
                        <li><a href="">New York</a></li>
                        <li><a href="">Florida</a></li>
                        <li><a href="">Michigan</a></li>
                        <li><a href="">Ohio</a></li>
                        <li><a href="">Chicago</a></li>
                        <li><a href="">Los Angeles</a></li>
                        <li><a href="">Maryland</a></li>
                        <li><a href="">Las Vegas</a></li>
                        <li><a href="">Indiana</a></li>
                        <li><a href="">Washington</a></li>
                        <li><a href="">San Francisco</a></li>
                        <li><a href="">Tennessee</a></li>
                        <li><a href="">Atlanta</a></li>
                        <li><a href="" class="more-city">more</a></li>
                      </ul>
                      <div class="hidden-div"></div>
                    </div>
                  </div>
                </li>
                <li class="flag">
                  <a href=""><img src="//cdn5.allevents.in/images/flags/32/BE.png" alt="United States"></a>
                  <div class="pop-cont">
                    <div class="pop-t">
                        <img src="//cdn5.allevents.in/images/flags/32/BE.png" alt="United States">
                        <span class="ml5">United States</span>
                    </div>
                    <div class="pop-c">
                      <ul class="city-list">
                        <li><a href="">New York</a></li>
                        <li><a href="">Florida</a></li>
                        <li><a href="">Michigan</a></li>
                        <li><a href="">Ohio</a></li>
                        <li><a href="">Chicago</a></li>
                        <li><a href="">Los Angeles</a></li>
                        <li><a href="">Maryland</a></li>
                        <li><a href="">Las Vegas</a></li>
                        <li><a href="">Indiana</a></li>
                        <li><a href="">Washington</a></li>
                        <li><a href="">San Francisco</a></li>
                        <li><a href="">Tennessee</a></li>
                        <li><a href="">Atlanta</a></li>
                        <li><a href="" class="more-city">more</a></li>
                      </ul>
                      <div class="hidden-div"></div>
                    </div>
                  </div>
                </li>
                <li class="flag">
                  <a href=""><img src="//cdn5.allevents.in/images/flags/32/JP.png" alt="United States"></a>
                  <div class="pop-cont">
                    <div class="pop-t">
                        <img src="//cdn5.allevents.in/images/flags/32/JP.png" alt="United States">
                        <span class="ml5">United States</span>
                    </div>
                    <div class="pop-c">
                      <ul class="city-list">
                        <li><a href="">New York</a></li>
                        <li><a href="">Florida</a></li>
                        <li><a href="">Michigan</a></li>
                        <li><a href="">Ohio</a></li>
                        <li><a href="">Chicago</a></li>
                        <li><a href="">Los Angeles</a></li>
                        <li><a href="">Maryland</a></li>
                        <li><a href="">Las Vegas</a></li>
                        <li><a href="">Indiana</a></li>
                        <li><a href="">Washington</a></li>
                        <li><a href="">San Francisco</a></li>
                        <li><a href="">Tennessee</a></li>
                        <li><a href="">Atlanta</a></li>
                        <li><a href="" class="more-city">more</a></li>
                      </ul>
                      <div class="hidden-div"></div>
                    </div>
                  </div>
                </li>
                <li class="flag">
                  <a href=""><img src="//cdn5.allevents.in/images/flags/32/IT.png" alt="United States"></a>
                  <div class="pop-cont">
                    <div class="pop-t">
                        <img src="//cdn5.allevents.in/images/flags/32/IT.png" alt="United States">
                        <span class="ml5">United States</span>
                    </div>
                    <div class="pop-c">
                      <ul class="city-list">
                        <li><a href="">New York</a></li>
                        <li><a href="">Florida</a></li>
                        <li><a href="">Michigan</a></li>
                        <li><a href="">Ohio</a></li>
                        <li><a href="">Chicago</a></li>
                        <li><a href="">Los Angeles</a></li>
                        <li><a href="">Maryland</a></li>
                        <li><a href="">Las Vegas</a></li>
                        <li><a href="">Indiana</a></li>
                        <li><a href="">Washington</a></li>
                        <li><a href="">San Francisco</a></li>
                        <li><a href="">Tennessee</a></li>
                        <li><a href="">Atlanta</a></li>
                        <li><a href="" class="more-city">more</a></li>
                      </ul>
                      <div class="hidden-div"></div>
                    </div>
                  </div>
                </li>
                <li class="flag">
                  <div class="pop-cont">
                    <div class="pop-t">
                        <span class="ml5">more</span>
                    </div>
                    <div class="pop-c">
                      <ul class="city-list">
                        <li><a href="">New York</a></li>
                        <li><a href="">Florida</a></li>
                        <li><a href="">Michigan</a></li>
                        <li><a href="">Ohio</a></li>
                        <li><a href="">Chicago</a></li>
                        <li><a href="">Los Angeles</a></li>
                        <li><a href="">Maryland</a></li>
                        <li><a href="">Las Vegas</a></li>
                        <li><a href="">Indiana</a></li>
                        <li><a href="">Washington</a></li>
                        <li><a href="">San Francisco</a></li>
                        <li><a href="">Tennessee</a></li>
                        <li><a href="">Atlanta</a></li>
                        <li><a href="" class="more-city">more</a></li>
                      </ul>
                      <div class="hidden-div"></div>
                    </div>
                  </div>
                </li> 
            </div>
          </ul>
        </div>
      </div>
    </div>        
  </section>
  <div style="border-bottom: 1px solid #ddd;padding: 30px 0;padding-bottom: 0;"></div>
  <section id="services">
    <div class="container">
      <h3 class="section-header">Worldwide Trending Events</h3>
      <div class="new-content row">
        @if((new \Jenssegers\Agent\Agent())->isDesktop())
          @php
            $posts = $postsPC;
          @endphp
          @else
          @php
            $posts = $postsSP;
          @endphp
        @endif
        @if(count($posts))
          @foreach($posts as $post)
            <div class="card-content col-lg-4 col-6">
              <div class="card-new-content">
                <a href="{{ route('frontend.post.show', $post->slug) }}">
                  <div class="card-new-content-img img-wrap" style="background: url('{{ ($post->featureImage) ? Helper::getMediaUrl($post->featureImage, 'medium') : Helper::getDefaultCover($post) }}');">
                  @if($post->featureImage)
                    <img src="{{ Helper::getMediaUrl($post->featureImage, 'medium') }}" />
                  @else
                    <img src="{{ Helper::getDefaultCover($post) }}">
                  @endif
                  </div>
                </a>

                <div class="post-man d-flex justify-content-between">
                  <label class="posted-date d-none d-md-block">{{ date('Y-m-d', strtotime($post->created_at)) }}</label>
                  <label class="card-category"><a href="{{ route('frontend.user.show', $post->user) }}">{{ $post->user->name }}</a></label>
                </div>
                <div class="artical-title">
                  <a href="{{ route('frontend.post.show', $post->slug) }}">{{ $post->title }}</a>
                </div>
              </div>
            </div>
          @endforeach
        @endif
      </div>
      <div class="row">
        <!-- pagination -->
        {{ $posts->appends(request()->query())->links('frontend.parts.pagination') }}
        <!--x-- pagination -->
      <!-- </div> -->
      <!---x--- card new content ---x--->
      </div>
    </div> 
  </section>
  <div style="border-bottom: 1px solid #ddd;padding: 30px 0;padding-bottom: 0;"></div>
  <div class="container">
    <div class="home-emanage">
      <div class="container">
        <div class="span6 right">
          <center>
            <img class="app_img_slider lazy" src="https://cdn-az.allevents.in/banners/6e4b88a070ab503b6842abd271269f13-rimg-w750-h750-dc202122-gmir.png" title="Discover Events On The GO" alt="Discover Events On The GO">
          </center>
        </div>
        <div class="span6 left">
          <h3 style="text-transform: none;">Discover Events On The GO</h3>
          <p>Find events happening around you while on the go! Our mobile apps are location aware and also allow exploring events by interest and navigation through your phone maps app. You can also set reminders, upload live photos for the world to see and share events to your social circle.</p>
          <h4>Get The Event Discovery App</h4>
          <a href="" target="_blank" style="margin-right: 10px;">
            <img class="lazy" width="150" height="50" src="//cdn.allevents.in/new/images/googleplay.png" alt="Download App From Google Play">
          </a>
          <a href="" target="_blank">
            <img class="lazy"  width="150" height="50" src="//cdn.allevents.in/new/images/appstore.png" alt="Download App From App Store">
          </a>
        </div>
      </div>
    </div>
  </div>
</main>
@endsection