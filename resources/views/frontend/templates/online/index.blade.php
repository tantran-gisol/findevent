@extends('frontend.layouts.app')
@section('content')
<header class="header" style="margin-top:60px;background-color: #20c0e8;height:40px !important;">
  <div class="container-fluid">
    <nav id="nav-menu-container" style="float: left;margin-left: 3%;">
      <ul class="nav-menu">
        <li><a href="" style="font-size:11px;text-transform:uppercase;">All</a></li>
        <li><a href="" style="font-size:11px;text-transform:uppercase;">Yoga</a></li>
        <li><a href="" style="font-size:11px;text-transform:uppercase;">Dance</a></li>
        <li><a href="" style="font-size:11px;text-transform:uppercase;">Cooking</a></li>
        <li><a href="" style="font-size:11px;text-transform:uppercase;">Business</a></li>
        <li><a href="" style="font-size:11px;text-transform:uppercase;">Meetups</a></li>
        <li><a href="" style="font-size:11px;text-transform:uppercase;">Heath & Wellness</a></li>
        <li><a href="" style="font-size:11px;text-transform:uppercase;">Virtual Run</a></li>
        <li><a href="" style="font-size:11px;text-transform:uppercase;">Meditation</a></li>
        <li><a href="" style="font-size:11px;text-transform:uppercase;">New Year</a></li>
        <li><a href="" style="font-size:11px;text-transform:uppercase;">Chirismas</a></li> 
        <li class="menu-has-children"><a href="" style="font-size:11px;text-transform:uppercase;">More</a>
          <ul style="background: #21c0e8;">
            <li><a href="" style="color:#fff">My Events</a></li>
            <li><a href="" style="color:#fff">Promote Events</a></li>
            <li><a href="" style="color:#fff">Sell Tickets Online</a></li>
            <li><a href="" style="color:#fff">Pricing Plans</a></li>
          </ul>
        </li>          
      </ul>
    </nav><!-- #nav-menu-container -->
  </div>
</header>
<div class="whitebar">
  <div class="calendar-nav">
    <div class="container">
      <ul class="datemenu">
        <li>
          <a href="" title="Events in Online in December 2021" class="dir wmd" style="cursor: pointer;">
            <span class="wmonth" style="visibility: visible;">Dec</span>
            <span  class="wyear" >2021</span>
          </a>  
        </li>
        <li style="margin-left: 10px;">
          <a rel="nofollow" href="" class="dir">
            <span class="wday" >T</span>
            <span class="day">14</span>
          </a>
          <!-- <ul>
            <li>
              <span class="btn-link ghref" data-href="">Christmas Concert ft. Maria Brannon and Caroline Sledge</span>
            </li>
            <li>
              <span class="btn-link ghref" data-href="">Virtual Sketchbook Club: Drawing Meaningful Objects</span>
            </li>
            <li>
              <span class="btn-link ghref" data-href="">The Write Time</span>
            </li>
            <li class="lastitem">
              <span class="btn-link ghref" data-href="">FREE - Michelangelo's Sistine Chapel Virtual Tour</span>
            </li>
            <li class="lastitem">
              <span class="btn-link ghref" data-href="">Show all events</span>
            </li>
          </ul> -->
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="" class="dir">
            <span class="wday" >T</span>
            <span class="day">15</span>
          </a>
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="" class="dir">
            <span class="wday" >T</span>
            <span class="day">16</span>
          </a>
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="" class="dir">
            <span class="wday" >T</span>
            <span class="day">17</span>
          </a>
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="" class="dir">
            <span class="wday" >T</span>
            <span class="day">18</span>
          </a>
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="" class="dir">
            <span class="wday" >T</span>
            <span class="day">19</span>
          </a>
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="" class="dir">
            <span class="wday" >T</span>
            <span class="day">20</span>
          </a>
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="" class="dir">
            <span class="wday" >T</span>
            <span class="day">21</span>
          </a>
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="" class="dir">
            <span class="wday" >T</span>
            <span class="day">22</span>
          </a>
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="" class="dir">
            <span class="wday" >T</span>
            <span class="day">23</span>
          </a>
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="" class="dir">
            <span class="wday" >T</span>
            <span class="day">25</span>
          </a>
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="" class="dir">
            <span class="wday" >T</span>
            <span class="day">26</span>
          </a>
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="" class="dir">
            <span class="wday" >T</span>
            <span class="day">27</span>
          </a>
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="" class="dir">
            <span class="wday" >T</span>
            <span class="day">28</span>
          </a>
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="" class="dir">
            <span class="wday" >T</span>
            <span class="day">29</span>
          </a>
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="" class="dir">
            <span class="wday" >T</span>
            <span class="day">30</span>
          </a>
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="" class="dir">
            <span class="wday" >T</span>
            <span class="day">31</span>
          </a>
        </li>
        <li style="margin-left:15px;">
          <a href="" title="Events in Online in December 2021" class="dir wmd" style="cursor: pointer;">
            <span class="wmonth" style="visibility: visible;">Dec</span>
            <span  class="wyear" >2021</span>
          </a>  
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="https://allevents.in/online/2021-12-14?ref=whitemenu"  class="dir "><span class="wday" >T</span><span class="day" >14</span></a>
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="https://allevents.in/online/2021-12-14?ref=whitemenu"  class="dir "><span class="wday" >T</span><span class="day" >15</span></a>
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="https://allevents.in/online/2021-12-14?ref=whitemenu"  class="dir "><span class="wday" >T</span><span class="day" >16</span></a>
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="https://allevents.in/online/2021-12-14?ref=whitemenu"  class="dir "><span class="wday" >T</span><span class="day" >17</span></a>
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="https://allevents.in/online/2021-12-14?ref=whitemenu"  class="dir "><span class="wday" >T</span><span class="day" >18</span></a>
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="https://allevents.in/online/2021-12-14?ref=whitemenu"  class="dir "><span class="wday" >T</span><span class="day" >19</span></a>
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="https://allevents.in/online/2021-12-14?ref=whitemenu"  class="dir "><span class="wday" >T</span><span class="day" >20</span></a>
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="https://allevents.in/online/2021-12-14?ref=whitemenu"  class="dir "><span class="wday" >T</span><span class="day" >21</span></a>
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="https://allevents.in/online/2021-12-14?ref=whitemenu"  class="dir "><span class="wday" >T</span><span class="day" >22</span></a>
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="https://allevents.in/online/2021-12-14?ref=whitemenu"  class="dir "><span class="wday" >T</span><span class="day" >23</span></a>
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="https://allevents.in/online/2021-12-14?ref=whitemenu"  class="dir "><span class="wday" >T</span><span class="day" >25</span></a>
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="https://allevents.in/online/2021-12-14?ref=whitemenu"  class="dir "><span class="wday" >T</span><span class="day" >26</span></a>
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="https://allevents.in/online/2021-12-14?ref=whitemenu"  class="dir "><span class="wday" >T</span><span class="day" >27</span></a>
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="https://allevents.in/online/2021-12-14?ref=whitemenu"  class="dir "><span class="wday" >T</span><span class="day" >28</span></a>
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="https://allevents.in/online/2021-12-14?ref=whitemenu"  class="dir "><span class="wday" >T</span><span class="day" >29</span></a>
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="https://allevents.in/online/2021-12-14?ref=whitemenu"  class="dir "><span class="wday" >T</span><span class="day" >30</span></a>
        </li>
        <li style="margin-left: 20px;">
          <a rel="nofollow" href="https://allevents.in/online/2021-12-14?ref=whitemenu"  class="dir "><span class="wday" >T</span><span class="day" >31</span></a>
        </li>
      </ul>
    </div>
  </div>
</div>

<section id="intro" style="margin-top:70px">
  <div class="intro-container">
    <div id="introCarousel" class="carousel  slide carousel-fade" data-ride="carousel">
      <div class="carousel-inner" role="listbox">
        <div class="carousel-item active" style="height:300px;">
          <div class="carousel-background">
            <img src="{{ asset('image/banner1.jpg') }}">
          </div>
          <div class="carousel-container">
            <div class="carousel-content">
              <h2>EVENTS IN HO CHI MINH CITY</h2>
              <p>This Vietnamese city is a very lively city with excellent food and various things to do. Although Vietnam is an Asian country but it is greatly influenced by the Europe as well which is evident from its language. One historical site that shouldn't be missed is the tunnel system of Vietnam created during Vietnam War by Viet Cong and played well in their favor. Visit the Cu Chi tunnels which is an intricate tunnel system consisting of 200miles of tunnels. During your visit, ou will also come across many tactics that the Viet Cong used during the war. The traffic in Vietnam is mad, but still it is pretty easy to cross the road as the motorists are expert in swerving around you so you feel liberating safe. The great festivals of the city are Nguyen Hue Flower Street Festival, Tet, King Hung's Commemoration, Buddha's Birthday, Book Reading Festival, Liberation Day, Southern Fruit Festival, National Day, and Dan Spring Flowers Festival. So, it's a city worth visiting.</p>
            </div>
          </div>   
        </div>
        <div>
            <div class="time-nav-head" style="text-shadow: none;">
              <ul class=""id="sidebar-days">
                <li>
                  <div class="cat-filt btn-group" style="margin-left: 10px;">
                    <a class="btn tltp dropdown-toggle" data-toggle="dropdown" href="#" style="border-radius: 20px;padding: 5px 15px;background: transparent; text-shadow: none; color: white;" data-original-title="" title="">Select Category
                      <span class="caret" style="line-height: 30px;margin-top: 9px;margin-left: 10px;float: right;border-top-color:white;"></span>
                    </a>
                  </div>
                </li>
              </ul>
            </div>
          </div>
      </div>
    </div>
  </div>
</section>

<section id="clients">
  <div class="row-fluid fold event-fold bgstyle-0 exhibitions-fold" id="exhibitions">
    <div class="container">
      <div class="events-style-resgrid" id="eh-348411144">
        <div class="head-label">
          <h2 class="headstyle-1 acolor">Exhibitions in 
            <span style="cursor: pointer; outline: none;text-decoration: underline;" class="city-select-action track" data-track="CityHomeFold|click|citychange">Ho Chi Minh City</span>
          </h2>
        </div>
        <div class="resgrid-row">
          <ul class="resgrid-ul">
            <li data-eid="10000244207981647" class="item ">
              <div class="thumb lazy" data-original="https://cdn-az.allevents.in/events5/banners/6e4694b86ccfde28580a7f344a05186ff872990b98e00821735d5e22419775b3-rimg-w1200-h600-gmir.jpg?v=1644222049" style="background-size:cover;overflow: unset !important;">
                <div class="ellipsis-btn-join-event"> 
                  <i title="I'm interested" data-eid="10000244207981647" data-track="CityHomeFold|click|Interested" class="icon-heart-empty event-interested-action track"></i>
                </div>
              </div>
              <div class="meta">
                <div> 
                  <div class="meta-left">
                    <span class="up-month">Jun</span>
                    <span class="up-day">09                                                         - 11                                                      
                    </span>
                  </div>
                  <div class="meta-right">
                    <div class="title" property="summary" style="word-wrap: break-word;">
                      <a href="https://allevents.in/ho%20chi%20minh%20city/telefilm-vietnam-2022-the-international-exhibition-on-film-and-television/10000244207981647?ref=cityhome" title="TELEFILM VIETNAM 2022 - THE INTERNATIONAL EXHIBITION ON FILM &amp; TELEVISION" target="_blank">
                        <h3 style="font-size: 0.75rem;">TELEFILM VIETNAM 2022 - THE INTERNATIONAL EXHIBITION ON FILM &amp; TELEVISION</h3>
                      </a>
                    </div>
                      <span class="up-venue toh">
                        SECC
                      </span>
                      <span class="up-time-display">
                        Thu Jun 09 2022 at 09:00 am
                      </span>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
            <div style="text-align: center;">
              <a class="btn btn-large tltp" href="" style="color: #555" data-original-title="View all Online Events">View all Online Events
                <i class="fas fa-angle-right"></i>
              </a> 
            </div>
        </div>
      </div>
    </div>
  </div>
</section>

<section id="clients">
  <div class="row-fluid fold event-fold bgstyle-0 exhibitions-fold" id="exhibitions">
    <div class="container">
      <div class="events-style-resgrid" id="eh-348411144">
        <div class="head-label">
          <h2 class="headstyle-1 acolor">Handpicked Online Events</h2>
        </div>
        <div class="resgrid-row">
          <ul class="resgrid-ul">
            <li data-eid="10000244207981647" class="item ">
              <div class="thumb lazy" data-original="https://cdn-az.allevents.in/events5/banners/6e4694b86ccfde28580a7f344a05186ff872990b98e00821735d5e22419775b3-rimg-w1200-h600-gmir.jpg?v=1644222049" style="background-size:cover;overflow: unset !important;">
                <div class="ellipsis-btn-join-event"> 
                  <i title="I'm interested" data-eid="10000244207981647" data-track="CityHomeFold|click|Interested" class="icon-heart-empty event-interested-action track"></i>
                </div>
              </div>
              <div class="meta">
                <div> 
                  <div class="meta-left">
                    <span class="up-month">Jun</span>
                    <span class="up-day">09                                                         - 11                                                      
                    </span>
                  </div>
                  <div class="meta-right">
                    <div class="title" property="summary" style="word-wrap: break-word;">
                      <a href="https://allevents.in/ho%20chi%20minh%20city/telefilm-vietnam-2022-the-international-exhibition-on-film-and-television/10000244207981647?ref=cityhome" title="TELEFILM VIETNAM 2022 - THE INTERNATIONAL EXHIBITION ON FILM &amp; TELEVISION" target="_blank">
                        <h3 style="font-size: 0.75rem;">TELEFILM VIETNAM 2022 - THE INTERNATIONAL EXHIBITION ON FILM &amp; TELEVISION</h3>
                      </a>
                    </div>
                      <span class="up-venue toh">
                        SECC
                      </span>
                      <span class="up-time-display">
                        Thu Jun 09 2022 at 09:00 am
                      </span>
                    </div>
                  </div>
                </div>
              </li>
            </ul>
            <div style="text-align: center;">
              <a class="btn btn-large tltp" href="" style="color: #555" data-original-title="View all Online Events">View all Online Events
                <i class="fas fa-angle-right"></i>
              </a> 
            </div>
        </div>
      </div>
    </div>
  </div>
</section>

<!-- <section id="clients">
  <div class="row-fluid fold event-fold bgstyle-0 exhibitions-fold" id="exhibitions">
    <div class="container">
      <div class="events-style-resgrid" id="eh-348411144">
        <div class="head-label">
          <h2 class="headstyle-1 acolor">Top Organizers From Ho Chi Minh City</h2>
        </div>
        <ul class="org-list">
          <li class="org-item orglink" data-org-id="1394188" data-href="https://allevents.in/org/idg-vietnam-public-sector/1394188?ref=cityhome-top-organizer" data-fcount="65" data-ecount="1" data-name="IDG Vietnam Public Sector" data-image="https://cdn-az.allevents.in/events6/banners/c3393d9887640e7af09ccb0cbea1323edc4a9c7e668e71b67d38a378e80ad14f-rimg-w400-h400-gmir.png?v=1583608788">
            <div class="left">
              <a href="https://allevents.in/org/idg-vietnam-public-sector/1394188?ref=cityhome-top-organizer" target="_blank">
                <img src="https://cdn-az.allevents.in/events6/banners/c3393d9887640e7af09ccb0cbea1323edc4a9c7e668e71b67d38a378e80ad14f-rimg-w400-h400-gmir.png?v=1583608788" alt="IDG Vietnam Public Sector">
              </a>
            </div>
            <div class="right">
              <div class="top">
                <div class="name">
                  <a href="https://allevents.in/org/idg-vietnam-public-sector/1394188?ref=cityhome-top-organizer" target="_blank" style="overflow-wrap: break-word;">IDG Vietnam Public Sector
                  </a>
                </div>
                <div class="f-btn">
                  <button class="btn btn-mini btn-warning btn-follow follow-action track" data-track="WhoToFollow|clicks|1394188" data-id="1394188" data-type="pop-org"><i class="icon-plus mr5"></i>Follow </button>
                </div>
              </div>
            </div>
          </li>
        </ul>
      </div>
    </div>
  </div>
</section> -->

<section id="clients" class="wow fadeInUp">
    <div class="container">
      <h3 class="section-header">Things To Do At Home</h3>
      <div class="row pop-cities">
        <div class="span2 ranm" style="background: url(//cdn2.allevents.in/transup/9e/9f5022cc7242e995fc8373bf6d51c7/online_health_and_wellness_yoga_allevents_in.jpg); background-size: cover;">
          <a href="" title="Online Health & Wellness Events">
            <div class="overlay">
              <p>Health & Wellness</p>
            </div>
          </a>
        </div>
        <div class="span2 ranm" style="background: url(////cdn2.allevents.in/transup/64/565a991e8347a7bf7ff5d51c603fa6/online_games_allevents_in.jpg); background-size: cover;">
          <a href="" title="Online Health & Wellness Events">
            <div class="overlay">
              <p>Games</p>
            </div>
          </a>
        </div>
        <div class="span2 ranm" style="background: url(//cdn-az.allevents.in/banners/c1d373a7f659665c228acd1e3e7398a9); background-size: cover;">
          <a href="" title="Online Health & Wellness Events">
            <div class="overlay">
              <p>Meetups</p>
            </div>
          </a>
        </div>
        <div class="span2 ranm" style="background: url(//cdn-az.allevents.in/banners/75547550c0e98622703cd444203cef3b); background-size: cover;">
          <a href="" title="Online Health & Wellness Events">
            <div class="overlay">
              <p>Music</p>
            </div>
          </a>
        </div>
      </div>
      <div class="row pop-cities">
        <div class="span2 ranm" style="background: url(//cdn2.allevents.in/transup/86/aba165bf804c4a9d9f1e554c526e31/online_art_events_allevents_in.jpg); background-size: cover;">
          <a href="" title="Online Health & Wellness Events">
            <div class="overlay">
              <p>Art</p>
            </div>
          </a>
        </div>
        <div class="span2 ranm" style="background: url(//cdn-az.allevents.in/banners/344231a933cb65fd654990cbd7edbc70); background-size: cover;">
          <a href="" title="Online Health & Wellness Events">
            <div class="overlay">
              <p>Food & Drinks</p>
            </div>
          </a>
        </div>
        <div class="span2 ranm" style="background: url(//cdn-az.allevents.in/banners/473e2e44128c7b0293eb02d7d5609edc); background-size: cover;">
          <a href="" title="Online Health & Wellness Events">
            <div class="overlay">
              <p>Business</p>
            </div>
          </a>
        </div>
        <div class="span2 ranm" style="background: url(//cdn-az.allevents.in/banners/f31adad8afa731706b70d8c28c95dccc); background-size: cover;">
          <a href="" title="Online Health & Wellness Events">
            <div class="overlay">
              <p>Sports</p>
            </div>
          </a>
        </div>
      </div>
      <div style="text-align: center;">
        <a class="btn btn-large tltp" href="" style="color: #555" data-original-title="View all Online Events">View all Online Events
          <i class="fas fa-angle-right"></i>
        </a> 
      </div>      
    </div>  
</section>

<div class="row-fluid fold  bgstyle-1">
  <div class="container-fluid">
    <div class="side-box-time span6" style="">
      <h4 style="font-size:16px;text-align: center;">Trending in Ho Chi Minh City</h4> 
      <ul class="side-box-list-time" id="sidebar-days">   
        <li><a class="" href="https://allevents.in/ho chi minh city/tin?ref=cityhome-trends" title="Tin events in Ho Chi Minh City" >Tin</a></li>
        <li><a class="" href="https://allevents.in/ho chi minh city/trong?ref=cityhome-trends" title="Trong events in Ho Chi Minh City" >Trong</a></li>
        <li><a class="" href="https://allevents.in/ho chi minh city/mang?ref=cityhome-trends" title="Mang events in Ho Chi Minh City" >Mang</a></li>
        <li><a class="" href="https://allevents.in/ho chi minh city/training?ref=cityhome-trends" title="Training events in Ho Chi Minh City" >Training</a></li>
        <li><a class="" href="https://allevents.in/ho chi minh city/exhibition?ref=cityhome-trends" title="Exhibition events in Ho Chi Minh City" >Exhibition</a></li>
        <li><a class="" href="https://allevents.in/ho chi minh city/dance?ref=cityhome-trends" title="Dance events in Ho Chi Minh City" >Dance</a></li>
      </ul>
    </div>  
    <div class="side-box-list-time nearby-cities span6" style="">
      <h4  style="font-size:16px">Events in Nearby Cities</h4>
      <li><a style="" href="https://allevents.in/ben tre?ref=cityhome-nearby-cities" title="Events in Ben Tre, Ben Tre, Vietnam">Ben Tre</a></li>
      <li><a style="" href="https://allevents.in/tây ninh?ref=cityhome-nearby-cities" title="Events in Tây Ninh, Tay Ninh, Vietnam">Tây Ninh</a></li>
    </div>
  </div>
</div>

<!-- <section id="clients">
  <div class="row-fluid fold subscribe-fold">
    <div class="container sub-cont">
      <div class="span6 left">
        <h3 class="headstyle-1 h2">Best of Ho Chi Minh City Events <b style=" font-family: pn-bold; ">in Your Inbox</b></h3>
        <p style=" font-size: 14px; ">
         Don't miss your favorite concert again. We deliver best of the city happenings and handpicked content for you every week. <b>Subscribe weekly email newsletter for Ho Chi Minh City.</b>
        </p>
      </div>
      <div class="span6 right pre-subscribe">
        <input class="email-input" name="email" style="" placeholder="Enter your email here" type="text">
        <button class="sub-action-btn btn btn-primary btn-large track" data-track="CityHomeSubscribe|clicks|Ho Chi Minh City" type="button" style="">Subscribe</button>
        <div class="subline">
          Join over a million newsletter subscribers.
        </div>
      </div>
    
      <div class="span6 right post-subscribe hide">
        <span class="t1">Thank You. We will keep you updated.</span>
        <p>Like us on Facebook and get interesting updates in your Feed.</p>
        <div class="like-container" class="mt20">
          <div class="fb-like" data-href="https://www.facebook.com/allevents.in" data-width="300" data-layout="standard" data-action="like" data-send="false" data-show-faces="true" data-share="false" style="overflow:hidden;"></div>
        </div>
      </div>
    </div>
  </div>
</section> -->
@endsection