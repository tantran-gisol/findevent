@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<!---------- admin-edit main section ---------->
	<section class="main">
		<div class="list-of-content-title">
			<h3>Cài đặt quản trị viên</h3>
			<p>Quản lý quản trị viên trang web và người tạo bài viết</p>

			@include('admin.parts.alert')
		</div>
		<a href="{{ route('admin.user.create') }}" class="btn" style="background: #111111; color: #ffffff; margin-top: 1rem; font-size: 12px;">
			<i class="fas fa-plus" style="width: 25px; color: #ffffff;"></i>Thêm quản trị viên
		</a>
		<div class="setting-table-list table-responsive-md">
			<table class="table">
			 <tr>
			 	<th style="width: 10%;"></th>
			   	<th style="width: 40%;">Tài khoản</th>
			    <th style="width: 15%;">Thẩm quyền</th>
			    <th style="width: 20%;">Ngày đăng ký / gia hạn</th>
			    <th style="width: 15%;">Ban quản lý</th>
			  </tr>
			  @if(count($users))
			  	@foreach($users as $user)
					  <tr>
					  	<td><!-- <img src="{{ !empty($user->avatar) ? $user->avatar : asset('image/no-img.png') }}" style=" width: 70px; height: 70px; border-radius: 5px;"> -->
						  <div class="d-block" style="width: 75px; height: 75px; margin: 0 auto;">
						  	<img src="{{ !empty($user->avatar) ? Helper::getImageUrl($user->avatar) : asset('image/no-img.png') }}" style="width: 100%; height: 100%; object-fit: cover; object-position: center; display: block; border-radius: 5px;">
						  </div>
						  </td>
					    <td>
					    	<p style=" color: black; font-size: 12px; margin-bottom: 30px;">u{{ $user->id }}<br>{{ $user->name }} < {{ $user->email }} ></p>
					    </td>
					    	<td><button class="btn" style="background: gray; color: #ffffff;width: 120px;">{{ $user->role->name }}</button></td>
					    <!-- <td style="color: #111111;"><p style="margin-bottom:50px; ">2019-06-26 11:30</p></td> -->
						<td style="color: #111111;"><p>{{ $user->updated_at }}</p></td>
					    <td>
							<div class="d-flex">
								<a href="{{ route('admin.user.edit', $user) }}" class="btn" style="background: #111111; color: #ffffff;width: 70px; font-size: 12px;">Chỉnh sửa</a>
								@if($user->id !== Auth::user()->site->user_id && $user->id !== Auth::user()->id)
								<a href="{{ route('admin.user.destroy', $user) }}" data-toggle="modal" data-target="#model-delete-user-{{ $user->id }}"><i class="fas fa-trash-alt" style="color: gray; margin-left: 26px; margin-top: 5px; font-size: 20px; vertical-align: middle;"></i></a>
								<div class="modal fade" id="model-delete-user-{{ $user->id }}" role="dialog" aria-hidden="true" aria-labelledby="modalDelLabel">
									<div class="modal-dialog" role="document">
										<div class="modal-content">
											<!-- <div class="modal-header">
												<h5 class="modal-title" id="modalDelLabel">Are you sure?</h5>
											</div> -->
											<div class="modal-body">
												<label class="mx-auto d-block lead text-center mb-5">{{ $user->name }} Bạn có chắc chắn muốn xóa?</label>
												<div class="d-flex justify-content-center">
													<button type="button" class="btn btn-secondary mr-2" data-dismiss="modal" style="width: 120px;">Hủy bỏ</button>
													<a href="{{ route('admin.user.destroy', $user) }}" id="btn-del" class="btn btn-dark" style="width: 120px;">Xóa bỏ</a>
												</div>
											</div>								
										</div>
									</div>
								</div>
								@endif

								<!-- modal delete -->
								
								
							</div>
					    </td>
					  </tr>
					@endforeach
				@endif
			</table>
		</div>
		<p style="color: #999999;">#{{ (($users->currentPage() - 1) * $users->perPage()) + 1 }}...#{{ ($users->currentPage() * $users->perPage()) <= $users->total() ? $users->currentPage() * $users->perPage() : $users->total() }} / {{ $users->total() }}</p>
		
		<div class="pagination-menu d-flex">
			{{ $users->onEachSide(1)->links() }}
		</div>
	</section>
@endsection
