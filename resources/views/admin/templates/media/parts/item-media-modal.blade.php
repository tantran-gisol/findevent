<div class="col-2">
  <a href="#" class="media-item" data-id="{{ $media->id }}" data-url="{{ Helper::getMediaUrl($media, 'original') }}" title="{{ $media->name }}"><img src="{{ Helper::getMediaUrl($media, 'thumbnail') }}" alt="" /></a>
</div>