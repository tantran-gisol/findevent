<div class="modal-header">
  <ul class="nav nav-tabs">
    <li class="nav-item">
      <a class="nav-link active" data-toggle="tab" href="#library-media" id="link-library-media">Media</a>
    </li>
    <li class="nav-item">
      <a class="nav-link" data-toggle="tab" href="#library-uploader">Uploader</a>
    </li>
  </ul>
</div>


<div class="modal-body">
  <div class="tab-content">
    <div id="library-media" class="tab-pane in active">
      <div class="row list-medias">
        @if(count($medias))
          @foreach($medias as $media)
            @include('admin.templates.media.parts.item-media-modal')
          @endforeach
        @endif
      </div>
    </div>
    <div id="library-uploader" class="tab-pane">
      <form id="form-library-uploader" action="{{ route('admin.media.modalUploadMedia') }}" method="POST" enctype="multipart/form-data">
        {{ csrf_field() }}
        <input type="file" name="file" required="" />
        <button type="submit" class="btn btn-no-radius" style="background: #111; color: #fff;">Upload</button>
      </form>
    </div>
  </div>
</div>
<div class="modal-footer">
  <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
</div>