@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<!---------- admin-edit main section ---------->
	<section class="main">
		<div class="list-of-content-title">
			<h3 class="font-weight-bold">Quản lý yêu cầu</h3>
			<h6 class="font-weight-bold">Kiểm tra nội dung được đăng bởi người dùng từ trang yêu cầu.</h6>
			@include('admin.parts.alert')
		</div>
		<div>
			<p style=" margin-bottom: 0;margin-top: 3.5rem;">Trang yêu cầu là "<span style="color:#C43638; ">Cài đặt phương tiện → Trang (mẫu liên hệ)</span>Có thể được tạo từ.</p>
			<p>Cài đặt đích thông báo cho các câu hỏi là "<span style="color:#C43638; ">Cài đặt tài khoản → Địa chỉ email hỗ trợ </span>Xác nhận</p>
		</div>
		<form action="{{ route('admin.inquiry.export') }}" method="GET">
			<button type="submit"><i class="fas fa-download" style="width: 25px; color: #111111;"></i>Tải xuống CSV</button>
		</form>
		<form action="{{ route('admin.inquiry.index') }}" method="get">
			<div class="col-12 dash-border-box" style="margin: 20px 0;">
				<div class="d-flex">
					<div style="margin: 1rem;margin-left: 10px;">
						<i class="fas fa-search" style="font-size: 24px; color: #d8dce3;"></i>
					</div>
					<div style="margin: .4rem 0;">
						<div style="margin-left: 2rem;">
							<div class="d-flex mb-1">
								<p class="font-weight-bold" style="width: 40%; text-align: right;">Địa chỉ email:</p>
								<input type="text" placeholder="user@example.com" class="form-control" name="email" value="{{ request()->email }}" style="margin-left: 15px; width: 70%; font-size: 12px;">
							</div>
							<div class="d-flex mb-1">
								<p class="font-weight-bold" style="width: 40%;text-align: right;">ID:</p>
								<input type="text" placeholder="123456" class="form-control" name="id" value="{{ request()->id }}" style="margin-left: 15px; width: 70%; font-size: 12px;">
							</div>
						</div>
						<div class="d-flex" style="margin-top: 30px;margin-bottom: 20px;">
							<input type="checkbox" class="checkbox" name="work_status[]" value="incompatible" {{ is_array(request()->work_status) && in_array('incompatible', request()->work_status) ? 'checked' : '' }}>
							<label for="checkbox" style="color:#3D507A;line-height: 1rem;">Không tương thích</label>
							<input type="checkbox" class="checkbox" name="work_status[]" value="acknowledged" {{ is_array(request()->work_status) && in_array('acknowledged', request()->work_status) ? 'checked' : '' }} style="margin-left: 1rem;">
							<label for="checkbox" style="color:#3D507A;line-height: 1rem;">Chấp nhận</label>
							<input type="checkbox" class="checkbox" name="work_status[]" value="ignore" {{ is_array(request()->work_status) && in_array('ignore', request()->work_status) ? 'checked' : '' }} style="margin-left: 1rem;">
							<label for="checkbox" style="color:#3D507A;line-height: 1rem;">Bỏ qua</label>
						</div>
						<button type="submit" class="btn btn-no-radius" style="background: #111111; color: #ffffff;width: 7rem;">Tìm kiếm</button>
					</div>
				</div>
			</div>
		</form>
		
		<p style="color: #999999;">#{{ (($inquiries->currentPage() - 1) * $inquiries->perPage()) + 1 }}...#{{ ($inquiries->currentPage() * $inquiries->perPage()) <= $inquiries->total() ? $inquiries->currentPage() * $inquiries->perPage() : $inquiries->total() }} / {{ $inquiries->total() }}</p>
		{{ $inquiries->links() }}

		<form action="{{ route('admin.inquiry.updateall') }}" method="post">
			{{ csrf_field() }}
			<div class="inquyri-table table-responsive-md">
				<table class="table">
					<tr>
						<th style="width:10px;"><input type="checkbox" name="" class="check-all"></th>
						<th style="width: 80px;">ID</th>
						<th style="width: 112px;">Ngày</th>
						<th style="width: 30%;">Địa chỉ email</th>
						<th>Nội dung</th>
						<th style="width: 6%;">Trạng thái</th>
						<th style="width: 8%;"></th>
					</tr>
					@if(count($inquiries))
						@foreach($inquiries as $inquiry)
							<tr>
								<td><input type="checkbox" class="check" name="ids[]" value="{{ $inquiry->id }}"></td>
								<td style="color: #111111;"><p style="margin-bottom: 50px;">{{ $inquiry->id }}</p></td>
								<td style="color: #111111;"><p style="margin-bottom: 50px;">{{ $inquiry->created_at }}</p></td>
								<td><p style="color:#C43638; margin-bottom: 30px;">{{ $inquiry->email }}</p></td>
								<td style="color: #C43638;"><p style="margin-bottom: 50px;">{{ $inquiry->content }}</p></td>
								<td style="color: #111111;"><p style="margin-bottom: 50px;">@if($inquiry->work_status == 'incompatible'){{ __('Không tương thích') }}@elseif($inquiry->work_status == 'ignore'){{ __('Bỏ qua') }}@else{{ __('Chấp nhận') }}@endif</p></td>
								<td><a href="{{ route('admin.inquiry.edit', $inquiry) }}" style="color: #C43638;"><p style="margin-bottom: 50px;"><i class="far fa-edit" style="color: #C43638;margin-right: 5px;"></i>Chi tiết</p></a></td>
							</tr>
						@endforeach
					@endif
				</table>
			</div>
			<div class="d-flex" style="margin-top: 1rem; margin-bottom: 1rem;">
				<select name="work_status" style="width: 225px;">
					<option value="acknowledged">Chấp nhận</option>
					<option value="incompatible">Không tương thích</option>
					<option value="ignore">Bỏ qua</option>
				</select>

				<button class="btn btn-no-radius" style="background: #111111; color: #ffffff;width:100px;margin-left: 5px;">Trong số các thẻ</button>
			</div>
		</form>
		<p style="color: #999999;">#{{ (($inquiries->currentPage() - 1) * $inquiries->perPage()) + 1 }}...#{{ ($inquiries->currentPage() * $inquiries->perPage()) <= $inquiries->total() ? $inquiries->currentPage() * $inquiries->perPage() : $inquiries->total() }} / {{ $inquiries->total() }}</p>
		<div class="pagination-menu d-flex">{{ $inquiries->onEachSide(1)->links() }}</div>
	</section>
	<!---------- admin-edit main section ---------->
@endsection