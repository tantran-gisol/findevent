@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<!---------- admin-edit main section ---------->
	<section class="main" >
		<div class="post-edit">
			<h3>Quản lý thẻ</h3>
      		<p style="margin-bottom: 0px;" >Quản lý các thẻ được sử dụng làm ứng viên đầu vào khi tạo bài viết.</p>
      		<p>Bằng cách đăng ký trước các thẻ, bạn có thể sử dụng chúng như các ứng cử viên nhập thẻ khi tạo bài viết.</p>

      @include('admin.parts.alert')

	</div>

	<!-- 12/02/2020 -->
    <!-- <div class="tag-text-management">
      <p style="color:#C43638; margin-bottom: 8px;">メディアのエディタ補完用タグ</p>
      <div class="button-tag-management d-flex" style="font-weight: bold;font-size: 12px;margin-left: 4px;margin-bottom: 5px;">
      	@if(count($menuTags))
      		@foreach( $menuTags as $menuTag )
	        	<a href="#" style="margin-right: 15px;"><button>{{ $menuTag->tag->name }}</button></a>
	        @endforeach
       	@endif
      </div>
      <p style="color: #c43638;"><i class="fas fa-caret-right" style="color: #c43638; width: 20px;"></i>メディアの非表示タグ</p>
	</div>
	 -->
	 <!-- x 12/02/2020 x -->
	 
    <div class="tag-management-table table-responsive-md">
			<table class="table">
			 <tr>
			   	<th>Hiển thị thứ tự</th>
			    <th style="width: 50%;">Thẻ tên</th>
			    <th style="width: 21%; min-width: 200px;"></th>
			  </tr>
			  @if(count($tags))
			  	@foreach($tags as $tag)
					  <tr>
					    <td>{{ $tag->order }}</td>
							<td>{{ $tag->name }}</td>
							<td>
								@if(!$loop->first)
									<a href="{{ route('admin.tag.reorder', [$tag, 'increase']) }}" style="margin-right: 5px;"><button><i class="fas fa-chevron-up" style="color:black;"></i></button></a>
								@endif
								@if(!$loop->last)
									<a href="{{ route('admin.tag.reorder', [$tag, 'decrease']) }}" style="{{ ($loop->first) ? 'margin-left: 44px; margin-right: 5px;' : 'margin-right: 5px;'}}"><button><i class="fas fa-chevron-down" style="color:black;"></i></button></a>
								@endif
								<a href="{{ route('admin.tag.edit', $tag) }}" style="{{ ($loop->last) ? 'margin-left: 44px;' : 'margin-left: 0;'}}"><button style="margin-top: 5px;"><i class="fa fa-pencil" style="color:black;"></i></button></a>
								<a href="{{ route('admin.tag.destroy', $tag) }}" style="margin-left: 0;"><button style="margin-top: 5px;"><i class="fas fa-trash-alt" style="color:black;"></i></button></a>
							</td> 
					  </tr>
				  @endforeach
			  @endif
			  <tr>
			  	<td colspan="4">
			  		<form action="{{ route('admin.tag.store') }}" method="post">
			  			{{ csrf_field() }}
			  			<input type="text" class="form-control" placeholder="Tên thẻ mới"name="name" value="{{ old('name') }}" style="display: inline-block; height: 35px; width:40%;font-size: 12px;">
				  		<button class="btn" style="background: #111111; width: 80px; color: #ffffff;height:32px;margin-bottom: 4px;border-radius: 0;font-size: 14px;"><i class="fas fa-plus" style="width: 25px; color: #ffffff;"></i>Thêm vào</button>
						</form>
			  	</td>
			  </tr>
			</table>
		</div>
		<div class="d-flex" style="margin-left: 38px; margin-top: 20px;" >
			<div>
				<i class="fas fa-info-circle" style="color:#98908A;"></i>
			</div>
			<div class="text-tag-management">
				<p>"Thẻ menu" là phương tiện(<a href="https://aidea.style/"><span>https://aidea.style/ </span></a>)Đã đăng ký làm menu <br>
				Đó là một thẻ đó là.<br>
				Giống như một thẻ thông thường, hãy sử dụng nó như một ứng cử viên nhập thẻ khi tạo một bài viết bằng trình chỉnh sửa<br>
				Nếu bạn muốn chỉnh sửa nội dung của thẻ menu, vui lòng thực hiện từ màn hình quản lý phương tiện.<br>
				</p>
			</div>
		</div>
	</section>
	<!---------- admin-edit main section ---------->
@endsection