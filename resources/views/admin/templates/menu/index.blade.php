@extends('admin.layouts.app')

@section('content')
	@include('admin.parts.left-menu')
	<!---------- admin-edit main section ---------->
	<section class="main">
		<div class="list-of-content-title">
			<h3>Quản lý thẻ menu</h3>
			<h6>Cài đặt cấu hình phương tiện và các chức năng thẻ đặc biệt。</h6>
			@include('admin.parts.alert')
		</div>
		@include('admin.templates.menu_tag.parts.menu-tabs')
		<section id="tab-menu-content">
			<div class="nav-tab-1-title">
				<p>Quản lý menu</p>
			</div>
			<div style="margin-top: 60px;">
				<div>
					<a href="{{ route('admin.menu.create') }}" class="btn" style="background: #111111; color: #ffffff;height:32px;margin-bottom: 4px;border-radius: 0;font-size: 12px;"><i class="fas fa-plus" style="width: 25px; color: #ffffff;"></i>Thêm mục menu</a>
				</div>
				<div class="col-12 dash-border-box p-3 my-4">
					<div>
						<form action="{{ route('admin.menu.index') }}">
							<select class="mb-1" name="location" style="width: 225px;">
								<option value="">Menu </option>
								@if(isset($menuLocations) && is_array($menuLocations))
									@foreach($menuLocations as $key => $location)
										<option value="{{ $key }}" {{ (request()->location == $key) ? 'selected' : '' }}>{{ $location }}</option>
									@endforeach
								@endif
							</select>
							<button class="btn btn-no-radius" style="background: #111111; color: #ffffff;width: 8rem;"><i class="fas fa-filter" style="color: #ffffff; width: 25px;"></i>Thu hẹp
							</button>
						</form>
					</div>
				</div>
			</div>
			<div class="menu-tag-management-table table-responsive-md" style="overflow: auto;">
				<table class="table">
					<tr>
					<th>Hiển thị thứ tự</th>
					<th>Tag tên</th>
					<th>Slug</th>
					<th>Menu</th>
					<th>Cài đặt hiển thị</th>
					<th style="width:100px;"></th>
					</tr>
					@if(count($menus))
						@foreach($menus as $menu)
							<tr>
								<td>{{ $loop->index+1 }}</td>
								<td>{{ $menu->name }}</td>
								<td>{{ $menu->url }}</td>
								<td>{{ array_key_exists($menu->location, $menuLocations) ? $menuLocations[$menu->location] : '' }}</td>
								<td>
									<form action="{{ route('admin.menu.updateshow', $menu) }}" class="form-menu-tag" method="POST">
										{{ csrf_field() }}
										<p>
											<label>
												<input type="checkbox" class="checkbox" name="show" value="1" {{ ($menu->show) ? 'checked' : '' }}>
												<span>Hiển thị</span>
											</label>
										</p>
									</form>
								</td>
								<td>
									<a href="{{ route('admin.menu.edit', $menu) }}"><i class="fa fa-pencil" style="color: gray; font-size: 20px; margin-right: 15px;" aria-hidden="true"></i></a>
									<a href="{{ route('admin.menu.destroy', $menu) }}"><i class="fas fa-trash-alt" style="color: gray; font-size: 20px;" aria-hidden="true"></i></a>
								</td>
							</tr>
						@endforeach
					@endif
				</table>	
			</div>
		</section>
		
	</section>
	<!---------- admin-edit main section ---------->
@endsection