@extends('auth.layouts.app')

@section('content')
    <div class="col-12">
        <div class="d-flex" style="background-color:#FAF7EA; padding: 5px;">
            <div>
                <i class="fas fa-exclamation-triangle" style="margin: 0 .5rem; color: #111111; font-size: 22px;"></i>
            </div>
            <div>
                <label class="forget-txt d-flex" style="font-size: 12px;">
                @if(session('status'))
                    {{ session('status') }}
                @else
                    {{ __('新しいバスワードをメールでお知らせします。再口グイン後、すぐにバスワードを変更してください。') }}
                @endif
                </label>
            </div>
        </div>
    </div>
    <form method="POST" action="{{ route('password.email') }}">
        @csrf
        <div class="admin-login-input col-12">
            <input type="email" placeholder="ログインメールアドレス" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required>
            @error('email')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror
        </div>
        <div class="col-12 text-center">
            <button class="btn btn-no-radius admin-login-btn w-100">{{ __('パスワードをリセット') }}</button>
        </div>
    </form>
    @if (Route::has('login'))
        <div class="col-12 text-center">
            <label class="forget-txt">
                <a href="{{ route('login') }}">{{ __('ログイン') }}</a>
            </label>
        </div>
    @endif
@endsection
