<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\PostView;
use App\Models\Media;
use Helper;

class PostController extends Controller
{
    protected $postModel;
    protected $postViewModel;
    protected $mediaModel;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct( Post $postModel, PostView $postViewModel, Media $mediaModel )
    {
        $this->postModel = $postModel;
        $this->postViewModel = $postViewModel;
        $this->mediaModel = $mediaModel;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    
    public function show(Request $request, $slug)
    {
        if($slug == 'admin'){
            return redirect()->route('admin.manager.index');
        }
        $post = $this->postModel->getPostBySlug($slug);
        if(!$post){
            abort(404);
        }
        $metaTitle = $metaDescription = $metaImage = '';
        $this->postViewModel->createViewLog($post);
        $relatedPosts = $this->postModel->getRelatedPost($post);
        if($request->has('view') && $request->view == 'embed'){
            return view('frontend.templates.post.embed')->with([
                'post' => $post,
                'relatedPosts' => $relatedPosts
            ]);
        }
        /*if($post->cover_image){
            $cover = $this->mediaModel->find($post->cover_image);
            $mainMedias = $post->medias->filter(function($media) use($post){
                return (int)$media->id !== (int)$post->cover_image;
            });
            if($cover){
                $mainMedias->prepend($cover);
            }
        }else{
            $mainMedias = $post->medias;
        }*/
        if($post->media_images && json_decode($post->media_images, true)){
            $mainMedias = json_decode($post->media_images, true);
        }else{
            $mainMedias = [];
        }
        if($post->ogp_title){
            $metaTitle = $post->ogp_title;
        }else{
            $metaTitle = $post->title;
        }
        if($post->ogp_description){
            $metaDescription = $post->ogp_description;
        }else{
            $metaDescription = strip_tags(Helper::getPostSummary($post->content));
        }
        if($post->ogp_image_url){
            $metaImage = $post->ogp_image_url;
        }else{
            if($post->featureImage){
                $metaImage = env('APP_URL', false) . Helper::getMediaUrl($post->featureImage, 'original');
            }elseif(Helper::getDefaultCover($post)){
                $metaImage = env('APP_URL', false) . Helper::getDefaultCover($post);
            }
        }
        return view('frontend.templates.post.show')->with([
            'post' => $post,
            'mainMedias' => $mainMedias,
            'relatedPosts' => $relatedPosts,
            'metaTitle' => $metaTitle,
            'metaDescription' => $metaDescription,
            'metaImage' => $metaImage
        ]);
    }
    public function preview(Request $request, $id)
    {
        $post = $this->postModel->findOrFail($id);
        $this->authorize('preview', $post);
        /*if($post->cover_image){
            $cover = $this->mediaModel->find($post->cover_image);
            $mainMedias = $post->medias->filter(function($media) use($post){
                return (int)$media->id !== (int)$post->cover_image;
            });
            if($cover){
                $mainMedias->prepend($cover);
            }            
        }else{
            $mainMedias = $post->medias;
        }*/
        if($post->media_images && json_decode($post->media_images, true)){
            $mainMedias = json_decode($post->media_images, true);
        }else{
            $mainMedias = [];
        }
        return view('frontend.templates.post.preview')->with([
            'post' => $post,
            'mainMedias' => $mainMedias
        ]);
    }

    public function album(Request $request, $slug)
    {
        if($slug == 'admin'){
            return redirect()->route('admin.manager.index');
        }
        $post = $this->postModel->getPostBySlug($slug);
        if(!$post){
            abort(404);
        }
        $this->postViewModel->createViewLog($post);
        /*if($post->cover_image){
            $cover = $this->mediaModel->find($post->cover_image);
            $mainMedias = $post->medias->filter(function($media) use($post){
                return (int)$media->id !== (int)$post->cover_image;
            });
            if($cover){
                $mainMedias->prepend($cover);
            }
        }else{
            $mainMedias = $post->medias;
        }*/
        if($post->media_images && json_decode($post->media_images, true)){
            $mainMedias = json_decode($post->media_images, true);
        }else{
            $mainMedias = [];
        }
        if(count($mainMedias) <= 0){
            return redirect()->route('frontend.post.show', $slug);
        }
        $metaTitle = $post->title;
        return view('frontend.templates.post.album')->with([
            'post' => $post,
            'mainMedias' => $mainMedias,
            'metaTitle' => $metaTitle
        ]);
    }
    
    public function submitVote(Request $request, $id)
    {
        $post = $this->postModel->findOrFail($id);
        $isVote = true;
        if($request->has('vote_opt') && !empty($request->vote_opt)){
            $voteData = $post->getVoteCountData();
            if(array_key_exists($request->vote_opt, $voteData)){
                $voteData[$request->vote_opt] = $voteData[$request->vote_opt] + 1;
                $post->vote_count = json_encode($voteData);
                $post->save();
            }
        }
        if($request->has('un_vote_opt') && !empty($request->un_vote_opt)){
            $voteData = $post->getVoteCountData();
            if(array_key_exists($request->un_vote_opt, $voteData)){
                $voteData[$request->un_vote_opt] = $voteData[$request->un_vote_opt] - 1;
                $post->vote_count = json_encode($voteData);
                $post->save();
            }
            $isVote = false;
        }
        return response()->json([
            'status' => true,
            'isVote' => $isVote,
            'html' => view('frontend.templates.post.parts.vote')->with([
                'post' => $post
            ])->render()
        ]);
    }
    public function loadMore(Request $request)
    {
        $post = $this->postModel->getLoadMorePost($request);
        if($post){
            $relatedPosts = $this->postModel->getRelatedPost($post);
            if($post->cover_image){
                $cover = $this->mediaModel->find($post->cover_image);
                $mainMedias = $post->medias->filter(function($media) use($post){
                    return (int)$media->id !== (int)$post->cover_image;
                });
                if($cover){
                    $mainMedias->prepend($cover);
                }
            }else{
                $mainMedias = $post->medias;
            }
            return response()->json([
                'status' => true,
                'post_id' => $post->id,
                'html' => view('frontend.templates.post.parts.content_post')->with([
                    'post' => $post,
                    'relatedPosts' => $relatedPosts,
                    'mainMedias' => $mainMedias
                ])->render()
            ]);
        }else{
            return response()->json([
                'status' => false
            ]);
        }
        
    }
    
}
