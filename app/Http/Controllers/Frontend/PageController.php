<?php

namespace App\Http\Controllers\Frontend;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Models\Page;
use App\Models\Setting;
use App\Models\Category;

class PageController extends Controller
{
    protected $pageModel;
    protected $settingModel;
    protected $categoryModel;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct( Page $pageModel, Setting $settingModel, Category $categoryModel )
    {
        $this->pageModel = $pageModel;
        $this->settingModel = $settingModel;
        $this->categoryModel = $categoryModel;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function show(Request $request, $slug)
    {
        $page = $this->pageModel->getPageBySlug($slug);
        if(!$page){
            abort(404);
        }
        $metaTitle = $page->title;
        if($slug == 'feature'){
            $featureCatId = $this->settingModel->getSettingData('siderbar_feature_cat_id');
            $categories = $this->categoryModel->where('parent_id', $featureCatId)->get();
            return view('frontend.templates.page.feature')->with([
                'page' => $page,
                'categories' => $categories,
                'metaTitle' => $metaTitle
            ]);
        }
        if($slug == 'series'){
            $seriesCatId = $this->settingModel->getSettingData('siderbar_series_cat_id');
            $categories = $this->categoryModel->where('parent_id', $seriesCatId)->get();
            return view('frontend.templates.page.series')->with([
                'page' => $page,
                'categories' => $categories,
                'metaTitle' => $metaTitle
            ]);
        }
        return view('frontend.templates.page.show')->with([
            'page' => $page,
            'metaTitle' => $metaTitle
        ]);
    }
    /*public function termOfService(Request $request)
    {
        return view('frontend.templates.page.term_of_service');
    }
    public function privacyPolicy(Request $request)
    {
        return view('frontend.templates.page.privacy_policy');
    }*/
    
}
