<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\PostSlider;
use App\Models\Media;
use Helper;

class PostSliderController extends Controller
{
    protected $postSliderModel;
    protected $mediaModel;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(PostSlider $postSliderModel, Media $mediaModel)
    {
        $this->postSliderModel = $postSliderModel;
        $this->mediaModel = $mediaModel;
    }

    public function uploadSliderImages(Request $request){
        $this->validate($request, [
            'files.*' => 'image|required|mimes:jpeg,png,jpg'
        ],[],[
            'files' =>  trans('table_fields.medias.files')
        ]);
        if(!$request->has('post_id') && !$request->has('slider_id')){
            return response()->json([
                'status' => false
            ]);
        }
        if(!$request->has('slider_id')){
            $slider = $this->postSliderModel->create(['post_id' => $request->post_id]);
            $isNewSlider = true;
        }else{
            $slider = $this->postSliderModel->find($request->slider_id);
            $isNewSlider = false;
        }
        $newPaths = [];
        foreach ($request->file('files') as $file) {
            $imageUploaded = Helper::uploadMedia($file);
            $mediaAttributes = [
                'name' => $imageUploaded['file_name'],
                'info' => $imageUploaded['info'],
                'post_id' => $slider->post_id
            ];
            foreach ($imageUploaded['size_paths'] as $key => $path) {
                $mediaAttributes[$key . '_path'] = $path;
            }
            $media = $this->mediaModel->create($mediaAttributes);
            $newPaths[$media->id] = Helper::getMediaUrl($media);
        }
        if(is_null(json_decode($slider->slide_images))){
            $slider->slide_images = json_encode($newPaths);
        }else{
            $slider->slide_images = json_encode( json_decode($slider->slide_images, true) + $newPaths );
        }
        $slider->save();
        if($isNewSlider){
            return response()->json([
                'status' => true,
                'slider_id' => $slider->id,
                'is_new_slider' => $isNewSlider,
                'slider' => view('admin.templates.post.parts.slider_item')->with([
                    'slider' => $slider
                ])->render()
            ]);
        }else{
            return response()->json([
                'status' => true,
                'slider_id' => $slider->id,
                'is_new_slider' => $isNewSlider,
                'images' => $newPaths
            ]);
        }
    }

    public function removeImage(Request $request, $id){
        if(!$request->has('media_id')){
            return response()->json([
                'status' => false
            ]);
        }
        $media_id = $request->media_id;
        $slider = $this->postSliderModel->findOrFail($id);
        $slide_images = json_decode($slider->slide_images, true);
        if(is_array($slide_images) && array_key_exists($media_id, $slide_images)){
            $media = $this->mediaModel->find($media_id);
            if($media){
                Helper::deleteMedia($media);
                $this->mediaModel->destroy($media_id);
            }            
            unset($slide_images[$media_id]);
            $slider->slide_images = json_encode( $slide_images );
            $slider->save();
        }
        return response()->json([
            'status' => true
        ]);
    }

    public function destroy(Request $request, $id){
        $slider = $this->postSliderModel->findOrFail($id);
        if(is_array(json_decode($slider->slide_images, true))){
            foreach(json_decode($slider->slide_images, true) as $media_id => $slide_image){
                $media = $this->mediaModel->find($media_id);
                Helper::deleteMedia($media);
                $this->mediaModel->destroy($media_id);
            }
        }
        $this->postSliderModel->destroy($id);
        return response()->json([
            'status' => true
        ]);
    }

}
