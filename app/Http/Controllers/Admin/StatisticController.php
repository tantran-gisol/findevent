<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;

use App\Models\Post;
use App\Models\PostView;
use App\Models\User;

class StatisticController extends Controller
{
    protected $postModel;
    protected $postViewModel;
    protected $userModel;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(Post $postModel, PostView $postViewModel, User $userModel)
    {
        $this->postModel = $postModel;
        $this->postViewModel = $postViewModel;
        $this->userModel = $userModel;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        if(Auth()->user()->role->slug == 'admin'){
            $posts = $this->postModel->getPostsByView($request);
            $totalView = $this->postViewModel->getPostTotalView();
        }else{
            $posts = $this->postModel->getPostsByView($request, Auth()->user()->id);
            $totalView = $this->postViewModel->getPostTotalView(Auth()->user()->id);
        }
        if(count($posts)){
            $postStatistic = $this->postViewModel->getViewLogByPost($posts->first());
        }else{
            $postStatistic = null;
        }
        return view('admin.templates.statistic.index')->with([
            'posts' => $posts,
            'totalView' => $totalView,
            'postStatistic' => $postStatistic
        ]);
    }
    public function user(Request $request)
    {
        $users = $this->userModel->getUsersByView($request);
        $totalView = $this->postViewModel->getUserTotalView();
        if(count($users)){
            $userStatistic = $this->postViewModel->getViewLogByUser($users->first());
        }else{
            $userStatistic = null;
        }
        return view('admin.templates.statistic.user')->with([
            'users' => $users,
            'totalView' => $totalView,
            'userStatistic' => $userStatistic
        ]);
    }
}
