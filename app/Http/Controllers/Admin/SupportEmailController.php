<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;
use App\Mail\MailApprovideSupport;
use Illuminate\Support\Facades\Mail;
use Illuminate\Http\Request;
use App\Models\SupportEmail;
use Illuminate\Support\Facades\Hash;

class SupportEmailController extends Controller
{
    protected $supportEmailModel;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct(SupportEmail $supportEmailModel)
    {
        $this->supportEmailModel = $supportEmailModel;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $this->authorize('index', $this->supportEmailModel);
        $supportEmails = $this->supportEmailModel->all();
        return view('admin.templates.support_email.index')->with([
            'supportEmails' => $supportEmails
        ]);
    }
    public function store(Request $request)
    {
        $this->authorize('store', $this->supportEmailModel);
        $this->validate($request, [
          'email' => 'required|email',
        ],[],[
            'email' =>  trans('table_fields.support_emails.email')
        ]);
        if($this->supportEmailModel->where('email', $request->email)->first()){
          return redirect()->route('admin.supportemail.index')->with('error', trans('messages.error.create', ['Module' => trans('module.entry.support_email')]));
        }
        $postData = $request->only(['email']);
        $postData['approvide_key'] = str_random(50);
        $supportEmail = $this->supportEmailModel->create($postData);
        if($supportEmail){
          Mail::to($supportEmail->email)->send(new MailApprovideSupport(route('admin.supportemail.approvide', $supportEmail->approvide_key) ));
        }
        return redirect()->route('admin.supportemail.index')->with('success', trans('messages.success.create', ['Module' => trans('module.entry.support_email')]));
    }
    public function approvide(Request $request, $code)
    {
        $supportEmail = $this->supportEmailModel->where('approvide_key', $code)->first();
        if($supportEmail){
          $supportEmail->is_approvide = 1;
          $supportEmail->save();
        }
        return redirect()->route('frontend.home.index');
    }
    public function destroy($id)
    {
        $supportEmail = $this->supportEmailModel->findOrFail($id);
        $this->authorize('destroy', $supportEmail);
        $this->supportEmailModel->destroy($id);
        return redirect()->route('admin.supportemail.index')->with('success', trans('messages.success.delete', ['Module' => trans('module.entry.support_email')]));
    }

}
