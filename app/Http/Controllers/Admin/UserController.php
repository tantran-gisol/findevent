<?php

namespace App\Http\Controllers\Admin;
use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Rules\OldPassword;
use App\Rules\UserEmail;
use App\Models\User;
use App\Models\Role;
use App\Models\PostView;
use Helper;
use Auth;

class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected $userModel;
    protected $roleModel;
    protected $postViewModel;
    public function __construct(User $userModel, Role $roleModel, PostView $postViewModel)
    {
        $this->userModel = $userModel;
        $this->roleModel = $roleModel;
        $this->postViewModel = $postViewModel;
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(Request $request)
    {
        $this->authorize('index', $this->userModel);
        $users = $this->userModel->paginate(20);
        return view('admin.templates.user.index')->with([
            'users' => $users
        ]);
    }

    public function indexOfficialWriter(Request $request)
    {
        $this->authorize('indexOfficialWriter', $this->userModel);
        $users = $this->userModel->getOfficialWriters();
        return view('admin.templates.user.index_official_writer')->with([
            'users' => $users
        ]);
    }

    public function create(Request $request)
    {
        $this->authorize('create', $this->userModel);
        $roles = $this->roleModel->all();
        return view('admin.templates.user.create')->with([
            'roles' => $roles
        ]);
    }

    public function store(Request $request)
    {
        $this->authorize('store', $this->userModel);
        $this->validate($request, [
            'email' => ['required','email', new UserEmail()],
            'password' => 'required|min:8|max:64|regex:/^.*(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?!.*[^a-zA-Z0-9]).*$/',
            'role_id' => 'required',
            'file' => 'image|mimes:jpeg,png,jpg'
        ],[],[
            'email' =>  trans('table_fields.users.email'),
            'password' =>  trans('table_fields.users.password'),
            'role_id' =>  trans('table_fields.users.role_id'),
            'file' =>  trans('table_fields.users.file'),
        ]);
        $postData = $request->only(
            'email',
            'name',
            'receive_notification',
            'writer_profile_html',
            'role_id',
            'disable_article_publishing'
        );
        if($request->has('password') && !empty($request->password)){
            $postData['password'] = bcrypt($request->password);
        }
        $postData['site_id'] = Auth::user()->site_id;
        $user = $this->userModel->create($postData);
        if($request->has('file')){
            $path = Helper::uploadAvatar($request->file);
            $user->avatar = $path;
            $user->save();
        }        
        return redirect()->route('admin.user.index')->with('success', trans('messages.success.create', ['Module' => trans('module.entry.user')]));
    }

    public function edit(Request $request, $id)
    {
        $user = $this->userModel->findOrFail($id);
        $this->authorize('edit', $user);
        $roles = $this->roleModel->all();
        return view('admin.templates.user.edit')->with([
            'user' => $user,
            'roles' => $roles
        ]);
    }

    public function update(Request $request, $id)
    {
        $user = $this->userModel->findOrFail($id);
        $this->authorize('update', $user);
        if($request->has('password') && empty($request->password)){
            unset($request['password']);
            unset($request['current_password']);
        }
        if(!$request->has('receive_notification')){
            $request->merge(['receive_notification'=>false]);
        }
        if(!$request->has('disable_article_publishing')){
            $request->merge(['disable_article_publishing'=>false]);
        }
        $this->validate($request, [
            'email' => ['required','email', new UserEmail($user)],
            'current_password' => ['required_with:password', new OldPassword($user)],
            'password' => 'nullable|confirmed|min:8|max:64|regex:/^.*(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?!.*[^a-zA-Z0-9]).*$/',
        ],[],[
            'email' =>  trans('table_fields.users.email'),
            'current_password' =>  trans('table_fields.users.current_password'),
            'password' =>  trans('table_fields.users.password')
        ]);
        $postData = $request->only(
            'email',
            'name',
            'receive_notification',
            'writer_profile_html',
            'role_id',
            'disable_article_publishing'
        );
        if($request->has('password') && !empty($request->password)){
            $postData['password'] = bcrypt($request->password);
        }
        $user->update($postData);
        return redirect()->route('admin.user.edit', [$user])->with('success', trans('messages.success.update', ['Module' => trans('module.entry.user')]));
    }

    public function updateAvatar(Request $request, $id)
    {
        $this->validate($request, [
            'file' => 'image|required|mimes:jpeg,png,jpg'
        ],[],[
            'file' =>  trans('table_fields.users.file')
        ]);
        $user = $this->userModel->findOrFail($id);
        $this->authorize('update_avatar', $user);
        if($user->avatar){
            Helper::deleteImage($user->avatar);
        }
        $path = Helper::uploadAvatar($request->file);
        $user->avatar = $path;
        $user->save();
        return redirect()->route('admin.user.edit', [$user])->with('success', trans('messages.success.update', ['Module' => trans('module.entry.user')]));
    }
    public function getViewLog(Request $request, $id){
        $user = $this->userModel->findOrFail($id);
        $userStatistic = $this->postViewModel->getViewLogByUser($user);
        return response()->json([
            'status' => true,
            'data' => $userStatistic
        ]);
    }
    public function destroy($id)
    {
        $user = $this->userModel->findOrFail($id);
        $this->authorize('destroy', $user);
        $this->userModel->destroy($id);
        return redirect()->route('admin.user.index')->with('success', trans('messages.success.delete', ['Module' => trans('module.entry.user')]));
    }
}
