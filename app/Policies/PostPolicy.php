<?php

namespace App\Policies;

use App\Models\User;
use App\Policies\AbstractPolicy\AbstractPolicy;

class PostPolicy extends AbstractPolicy {
    protected $model = 'post';

    public function index(User $user) {
        return $user->ability('admin.' . $this->model . '.index');
    }

    public function indexBySite(User $user) {
        return $user->ability('admin.' . $this->model . '.indexBySite');
    }

    public function edit(User $user, $item) {
        if($user->role->slug == 'admin'){
            return $user->ability('admin.' . $this->model . '.edit');
        }else{
            return ($item->user_id == $user->id) ? true : false;
        }
    }

    public function create(User $user) {

        return $user->ability('admin.' . $this->model . '.create');
    }

    public function update(User $user, $item) {
        return $user->ability('admin.' . $this->model . '.update');
    }

    public function store(User $user) {
        return $user->ability('admin.' . $this->model . '.store');
    }

    public function destroy(User $user, $item) {
        if($user->role->slug == 'admin'){
            return $user->ability('admin.' . $this->model . '.destroy');
        }else{
            return ($item->user_id == $user->id) ? true : false;
        }
        
    }

    /* Frontend */
    public function preview(User $user, $item) {
        if($user->role->slug == 'admin'){
            return $user->ability('frontend.' . $this->model . '.preview');
        }else{
            return ($item->user_id == $user->id) ? true : $item->previewers->contains('id', $user->id) ;
        }
    }
}
