<?php

namespace App\Policies;

use App\Models\User;
use App\Policies\AbstractPolicy\AbstractPolicy;

class SitePolicy extends AbstractPolicy {
    protected $model = 'site';

    public function index(User $user) {
        return $user->ability('admin.' . $this->model . '.index');
    }

    public function edit(User $user, $item) {
        return $user->ability('admin.' . $this->model . '.edit');
    }

    public function create(User $user) {

        return $user->ability('admin.' . $this->model . '.create');
    }

    public function update(User $user, $item) {
        return $user->ability('admin.' . $this->model . '.update');
    }

    public function store(User $user) {
        return $user->ability('admin.' . $this->model . '.store');
    }

    public function destroy(User $user, $item) {
        return $user->ability('admin.' . $this->model . '.destroy');
    }
}
