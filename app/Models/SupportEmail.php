<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class SupportEmail extends Model
{
	protected $fillable = [
		'email', 'is_approvide', 'approvide_key'
	];
}
