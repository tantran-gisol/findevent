<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
  protected $fillable = [
    'name', 'order', 'featured_image', 'sub_featured_image'
  ];
  public function posts()
  {
    return $this->belongsToMany(Post::class);
  }

  public function createTag(array $tagData){
    $tagAttributes = $tagData;
    $tagAttributes['order'] = $this->max('order') + 1;
    $tag = $this->create($tagAttributes);
    return $tag;
  }
  public function createTags(array $tagNames){
    $maxOrder = $this->max('order') + 1;
    $tagIDs = [];
    foreach ($tagNames as $key => $tagName) {
      if($this->where('name', trim($tagName))->exists()){
        $tag = $this->where('name', trim($tagName))->first();
        $tagIDs[] = $tag->id;
      }else{
        $tagAttributes = [
          'name' => trim($tagName),
          'order' => $maxOrder++
        ];
        $tag = $this->create($tagAttributes);
        $tagIDs[] = $tag->id;
      }
    }
    $tags = $this->whereIn('id', $tagIDs)->orderBy('id', 'desc')->get();
    return $tags;
  }
}