<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Lienhe extends Model
{
	public $table = "lienhe";
    public $fillable = ['name', 'email', 'subject', 'message', 'created_at', 'updated_at'];
}