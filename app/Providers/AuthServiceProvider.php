<?php

namespace App\Providers;

//Models
use App\Models\Inquiry;
use App\Models\Media;
use App\Models\MenuTag;
use App\Models\Menu;
use App\Models\Post;
use App\Models\Page;
use App\Models\Setting;
use App\Models\Site;
use App\Models\SupportEmail;
use App\Models\Tag;
use App\Models\Category;
use App\Models\User;
use App\Models\Vote;
//Policies
use App\Policies\InquiryPolicy;
use App\Policies\MediaPolicy;
use App\Policies\MenuTagPolicy;
use App\Policies\MenuPolicy;
use App\Policies\PostPolicy;
use App\Policies\PagePolicy;
use App\Policies\SettingPolicy;
use App\Policies\SitePolicy;
use App\Policies\SupportEmailPolicy;
use App\Policies\TagPolicy;
use App\Policies\CategoryPolicy;
use App\Policies\UserPolicy;
use App\Policies\VotePolicy;

use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        Inquiry::class => InquiryPolicy::class,
        Media::class => MediaPolicy::class,
        MenuTag::class => MenuTagPolicy::class,
        Menu::class => MenuPolicy::class,
        Post::class => PostPolicy::class,
        Page::class => PagePolicy::class,
        Setting::class => SettingPolicy::class,
        Site::class => SitePolicy::class,
        SupportEmail::class => SupportEmailPolicy::class,
        Tag::class => TagPolicy::class,
        Category::class => CategoryPolicy::class,
        User::class => UserPolicy::class,
        Vote::class => VotePolicy::class,
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //
    }
}
