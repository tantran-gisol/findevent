<?php
/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::namespace ('Admin')->group(function () {
	Route::middleware(['auth'])->group(function(){
		Route::get('dashboard', 'ManagerController@index')->name('admin.manager.index');
		Route::prefix('user')->group(function(){
			Route::get('/', 'UserController@index')->name('admin.user.index');
			Route::get('/edit/{id}', 'UserController@edit')->name('admin.user.edit');
			Route::get('/create', 'UserController@create')->name('admin.user.create');
			Route::post('/store', 'UserController@store')->name('admin.user.store');
			Route::post('/update/{id}', 'UserController@update')->name('admin.user.update');
			Route::post('/update_avatar/{id}', 'UserController@updateAvatar')->name('admin.user.update_avatar');
			Route::get('/get-view-log/{id}', 'UserController@getViewLog')->name('admin.user.get_view_log');
			Route::get('destroy/{id}','UserController@destroy')->name('admin.user.destroy');
		});
		Route::prefix('category')->group(function(){
			Route::get('/', 'CategoryController@index')->name('admin.category.index');
			Route::get('/create', 'CategoryController@create')->name('admin.category.create');
			Route::post('/store', 'CategoryController@store')->name('admin.category.store');
			Route::get('/edit/{id}', 'CategoryController@edit')->name('admin.category.edit');
			Route::post('/update/{id}', 'CategoryController@update')->name('admin.category.update');
			Route::get('/destroy/{id}','CategoryController@destroy')->name('admin.category.destroy');
		});
		Route::prefix('tag')->group(function(){
			Route::get('/', 'TagController@index')->name('admin.tag.index');
			Route::post('/store', 'TagController@store')->name('admin.tag.store');
			Route::get('/edit/{id}', 'TagController@edit')->name('admin.tag.edit');
			Route::post('/update/{id}', 'TagController@update')->name('admin.tag.update');
			Route::get('re-order/{id}/{action}','TagController@reOrder')->name('admin.tag.reorder');
			Route::get('destroy/{id}','TagController@destroy')->name('admin.tag.destroy');
		});
		Route::prefix('post')->group(function(){
			Route::get('/', 'PostController@index')->name('admin.post.index');
			Route::get('/create', 'PostController@create')->name('admin.post.create');
			Route::get('/get-view-log/{id}', 'PostController@getViewLog')->name('admin.post.get_view_log');
			Route::get('/preedit/{id}', 'PostController@preedit')->name('admin.post.preedit');
			Route::post('/pre-update/{id}', 'PostController@preUpdate')->name('admin.post.preupdate');
			Route::get('/edit/{id}', 'PostController@edit')->name('admin.post.edit');
			Route::post('/upload-feature-image/{id}', 'PostController@uploadFeatureImage')->name('admin.post.uploadfeatureimage');
			Route::get('/modal-get-medias/{id}', 'PostController@modalGetMedias')->name('admin.post.modalGetMedias');
			Route::post('/update-media-images/{id}', 'PostController@updateMediaImages')->name('admin.post.updatemediaimages');
			Route::post('/add-tags', 'PostController@addTags')->name('admin.post.addtags');
			Route::post('/add-previewer/{id}', 'PostController@addPreviewer')->name('admin.post.addpreviewer');
			Route::post('/update-feature-image/{id}', 'PostController@updateFeatureImage')->name('admin.post.updatefeatureimage');
			Route::post('/update-feature-style/{id}', 'PostController@updateFeatureStyle')->name('admin.post.updatefeaturestyle');
			Route::post('/update/{id}', 'PostController@update')->name('admin.post.update');
			Route::post('/update-all', 'PostController@changeAll')->name('admin.post.changeAll');
			Route::get('/destroy/{id}', 'PostController@destroy')->name('admin.post.destroy');
		});
		Route::prefix('slider')->group(function(){
			Route::post('/upload-images', 'PostSliderController@uploadSliderImages')->name('admin.slider.uploadSliderImages');
			Route::post('/remove-image/{id}', 'PostSliderController@removeImage')->name('admin.slider.removeImage');
			Route::get('/destroy/{id}', 'PostSliderController@destroy')->name('admin.slider.destroy');
		});
		Route::prefix('page')->group(function(){
			Route::get('/create', 'PageController@create')->name('admin.page.create');
			Route::post('/store', 'PageController@store')->name('admin.page.store');
			Route::get('/edit/{id}', 'PageController@edit')->name('admin.page.edit');
			Route::post('/update/{id}', 'PageController@update')->name('admin.page.update');
			Route::get('/change-status/{id}', 'PageController@changeStatus')->name('admin.page.change_status');
		});
		Route::prefix('site')->group(function(){
			Route::get('/edit/{id}', 'SiteController@edit')->name('admin.site.edit');
			Route::post('/update/{id}', 'SiteController@update')->name('admin.site.update');
			Route::prefix('post')->group(function(){
				Route::get('/', 'PostController@indexBySite')->name('admin.post.indexBySite');
			});
		});
		Route::prefix('media')->group(function(){
			Route::get('/', 'MediaController@index')->name('admin.media.index');
			Route::post('/store', 'MediaController@store')->name('admin.media.store');
			Route::post('/ckeditor-post-upload-image/{id}', 'MediaController@ckeditorPostUploadImage')->name('admin.media.ckeditorpostuploadimage');
			Route::get('/destroy/{id}', 'MediaController@destroy')->name('admin.media.destroy');
			//modal library
			Route::get('/modal-get-medias', 'MediaController@modalGetMedias')->name('admin.media.modalGetMedias');
			Route::post('/modal-upload-media', 'MediaController@modalUploadMedia')->name('admin.media.modalUploadMedia');
			Route::get('/ckeditor-connector-action/{id}', 'MediaController@ckeditorConnectorAction')->name('admin.media.ckeditorConnectorAction');
			Route::post('/ckeditor-connector-action/{id}', 'MediaController@ckeditorConnectorAction')->name('admin.media.ckeditorConnectorAction');
		});
		//Setting
		Route::prefix('setting')->group(function(){
			Route::get('/', 'SettingController@index')->name('admin.setting.index');
			Route::post('/update', 'SettingController@update')->name('admin.setting.update');
			Route::get('widget', 'SettingController@widget')->name('admin.setting.widget');
			Route::get('fixedpage', 'SettingController@fixedpage')->name('admin.setting.fixedpage');
			Route::get('vote', 'SettingController@vote')->name('admin.setting.vote');
			Route::get('sidebar', 'SettingController@sidebar')->name('admin.setting.sidebar');
			Route::get('stylesheet', 'SettingController@stylesheet')->name('admin.setting.stylesheet');
			Route::get('footer', 'SettingController@footer')->name('admin.setting.footer');
		});
		//Vote
		Route::prefix('vote')->group(function(){
			Route::get('/create', 'VoteController@create')->name('admin.vote.create');
			Route::post('/store', 'VoteController@store')->name('admin.vote.store');
			Route::get('/edit/{id}', 'VoteController@edit')->name('admin.vote.edit');
			Route::post('/update/{id}', 'VoteController@update')->name('admin.vote.update');
			Route::get('/destroy/{id}', 'VoteController@destroy')->name('admin.vote.destroy');
		});
		Route::prefix('official-writer')->group(function(){
			Route::get('/', 'UserController@indexOfficialWriter')->name('admin.user.indexofficialwriter');
		});
		Route::prefix('inquiry')->group(function(){
			Route::get('/', 'InquiryController@index')->name('admin.inquiry.index');
			Route::get('/edit/{id}', 'InquiryController@edit')->name('admin.inquiry.edit');
			Route::post('/update/{id}', 'InquiryController@update')->name('admin.inquiry.update');
			Route::post('/update-all', 'InquiryController@updateAll')->name('admin.inquiry.updateall');
			Route::get('/export', 'InquiryController@export')->name('admin.inquiry.export');
		});
		Route::prefix('email-support')->group(function(){
			Route::get('/', 'SupportEmailController@index')->name('admin.supportemail.index');
			Route::post('/store', 'SupportEmailController@store')->name('admin.supportemail.store');
			Route::get('/approvide/{code}', 'SupportEmailController@approvide')->name('admin.supportemail.approvide');
			Route::get('/destroy/{id}', 'SupportEmailController@destroy')->name('admin.supportemail.destroy');
		});
		//MenuTagController
		Route::prefix('menu-tag')->group(function(){
			Route::get('/', 'MenuTagController@index')->name('admin.menu_tag.index');
			Route::post('/store', 'MenuTagController@store')->name('admin.menu_tag.store');
			Route::post('/update/{id}', 'MenuTagController@update')->name('admin.menu_tag.update');
			Route::get('/destroy/{id}', 'MenuTagController@destroy')->name('admin.menu_tag.destroy');
		});
		//MenuController
		Route::prefix('menu')->group(function(){
			Route::get('/', 'MenuController@index')->name('admin.menu.index');
			Route::get('/create', 'MenuController@create')->name('admin.menu.create');
			Route::post('/store', 'MenuController@store')->name('admin.menu.store');
			Route::get('/edit/{id}', 'MenuController@edit')->name('admin.menu.edit');
			Route::post('/update/{id}', 'MenuController@update')->name('admin.menu.update');
			Route::post('/update-show/{id}', 'MenuController@updateshow')->name('admin.menu.updateshow');
			Route::get('/destroy/{id}', 'MenuController@destroy')->name('admin.menu.destroy');
		});
		//Statistic
		Route::prefix('statistic')->group(function(){
			Route::get('/', 'StatisticController@index')->name('admin.statistic.index');
			Route::get('user', 'StatisticController@user')->name('admin.statistic.user');
		});
		//Statistic
		Route::prefix('google-statistic')->group(function(){
			Route::get('/', 'GoogleStatisticController@index')->name('admin.google_statistic.index');
			Route::post('/', 'GoogleStatisticController@index')->name('admin.google_statistic.index');
		});
	});
});